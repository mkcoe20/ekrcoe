package service;

import it.ekr.ekrcoe.operations.OdataFunctionsExtension;

import javax.persistence.EntityManagerFactory;

import org.apache.olingo.odata2.jpa.processor.api.ODataJPAContext;
import org.apache.olingo.odata2.jpa.processor.api.ODataJPAServiceFactory;
import org.apache.olingo.odata2.jpa.processor.api.exception.ODataJPARuntimeException;
import org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmExtension;

/**
 * Odata JPA Processor implementation class
 */
public class EkrCoeServiceFactory extends ODataJPAServiceFactory {

	

	@Override
	public ODataJPAContext initializeODataJPAContext()
			throws ODataJPARuntimeException {
		ODataJPAContext oDataJPAContext = this.getODataJPAContext();
		try {
			EntityManagerFactory emf = JpaEntityManagerFactory
					.getEntityManagerFactory();
			oDataJPAContext.setEntityManagerFactory(emf);
			oDataJPAContext.setPersistenceUnitName(Source.PERSISTENCE_UNIT_NAME);
			oDataJPAContext.setJPAEdmMappingModel("EkrCoeEdmMapping.xml");
			oDataJPAContext.setJPAEdmExtension((JPAEdmExtension)new OdataFunctionsExtension());
			return oDataJPAContext;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
