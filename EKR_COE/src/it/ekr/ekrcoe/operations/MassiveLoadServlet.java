package it.ekr.ekrcoe.operations;

import it.ekr.ekrcoe.jpa.CsvField;
import it.ekr.ekrcoe.jpa.CsvFieldPK;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.NamingException;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TransactionRequiredException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;

import service.JpaEntityManagerFactory;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.reflect.*;
import com.sap.security.um.user.PersistenceException;

/**
 * Servlet implementation class MassiveLoadServlet
 */
@WebServlet("/MassiveLoadServlet")
public class MassiveLoadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private EntityManager em;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MassiveLoadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		BufferedReader reader = request.getReader();
	    Gson gson = new Gson();
	    Type type = new TypeToken<Collection<CsvField>>(){}.getType();
	    Collection<CsvField> collectionSent = gson.fromJson(reader, type);
	    EntityTransaction tx = em.getTransaction();
	    tx.begin();
	    //prima devo vedere se ci sono gi� valori ed in caso devo fare il persist diretto
	    long count = 0;
	    try
	    {
		    for (CsvField field:collectionSent)
		    {
		       //em.merge(field); //con il merge da problemi dopo la delete
		        CsvFieldPK primaryKey = new CsvFieldPK();
		        primaryKey.setFilePath(field.getFilePath());
		        primaryKey.setLabel(field.getLabel());
		        primaryKey.setMandt(field.getMandt());
		        primaryKey.setRowNumber(field.getRowNumber());
		        
		       CsvField foundIn = em.find(CsvField.class, primaryKey);
		       if (foundIn == null)
		       {
		    	   em.persist(field);
		       }else
		       {
		    	   em.merge(field);
		       }
		       
		    	
		    	count ++;
//		    	if (em.contains(field))
//		    	{
//		    		em.refresh(field);
//		    	}else
//		    	{
//		    		em.persist(field);
//		    	}
		    	
		    }
		    em.flush();
		    tx.commit();
	    }catch (Throwable e)
	    {
	    	if (tx.isActive())
	    	{
	    		tx.rollback();
	    	}
	    	throw new ServletException(e);
	    }

	    
	    //em.clear();
	    response.setStatus(HttpStatus.SC_OK);
	    PrintWriter writer = response.getWriter();
	    writer.println("{\"writtenValues\":"+count+"}");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException
	{
		super.init();
		
		 try
		{
			em = JpaEntityManagerFactory.getEntityManager();
		} catch (NamingException | SQLException e)
		{
			throw new ServletException(e);
		}
	}

}
