/**
 * 
 */
package it.ekr.ekrcoe.operations;

import java.io.InputStream;

import org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmExtension;
import org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmSchemaView;

/**
 * @author Marco Scarpa
 *
 */
public class OdataFunctionsExtension implements JPAEdmExtension
{

	/* (non-Javadoc)
	 * @see org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmExtension#extendJPAEdmSchema(org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmSchemaView)
	 */
	@Override
	public void extendJPAEdmSchema(JPAEdmSchemaView arg0)
	{
		// TODO Auto-generated method stub
		int a =0;

	}

	/* (non-Javadoc)
	 * @see org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmExtension#extendWithOperation(org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmSchemaView)
	 */
	@Override
	public void extendWithOperation(JPAEdmSchemaView view)
	{
		view.registerOperations(OdataFunctions.class, null);

	}

	/* (non-Javadoc)
	 * @see org.apache.olingo.odata2.jpa.processor.api.model.JPAEdmExtension#getJPAEdmMappingModelStream()
	 */
	@Override
	public InputStream getJPAEdmMappingModelStream()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
