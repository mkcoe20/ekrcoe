package it.ekr.ekrcoe.operations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpConnection;
import org.apache.olingo.odata2.api.annotation.edm.EdmFunctionImport.HttpMethod;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.FileList;
import com.google.gdata.client.http.GoogleGDataRequest.GoogleCookie;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.Cell;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import service.JpaEntityManagerFactory;

/**
 * Servlet implementation class GoogleSheetUpload
 */
@WebServlet(description = "servlet to upload google spreadsheet, with post you insert a json with spreadsheet code, username and password", urlPatterns = { "/GoogleSheetUpload" })
public class GoogleSheetUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private EntityManager em;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GoogleSheetUpload() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		
		super.init(config);
		try
		{
			em=JpaEntityManagerFactory.getEntityManager();
		} catch (NamingException | SQLException e)
		{
			throw new ServletException(e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * @see Servlet#getServletConfig()
	 */
	public ServletConfig getServletConfig() {
		
		return super.getServletConfig();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.service(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try
		{
			ServletOutputStream out = response.getOutputStream();
			BufferedReader reader = request.getReader();
			JsonParser parser = new JsonParser();
			JsonElement parsedJson = parser.parse(reader);
			JsonObject obj = parsedJson.getAsJsonObject();
			JsonElement spreadsheetIDElem = obj.get("spreadsheetID");
			String spreadsheetId = spreadsheetIDElem.getAsString();
			JsonElement usernameElem= obj.get("username");
			String username = usernameElem.getAsString();
//			JsonElement passwordElem= obj.get("password");
//			String password = passwordElem.getAsString();
			SpreadsheetService spreadsheetService = new SpreadsheetService("ekrprovatest");
			spreadsheetService.setProtocolVersion(SpreadsheetService.Versions.V3);
			
			InputStream in = getServletContext().getResourceAsStream("ekrprovatest-4679bc4b7c45.json");
			String p12Paths = getServletContext().getRealPath("90212f141d751c54273dba12ae701aba2986a7df-privatekey.p12");
			JsonFactory factory = JacksonFactory.getDefaultInstance();
			GoogleClientSecrets secrets = GoogleClientSecrets.load(factory, new InputStreamReader(in));
			HttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
			List<String> scopes = new ArrayList<String>();
			String  spreadsheetScope= "https://spreadsheets.google.com/feeds";
			//scopes.add(DriveScopes.DRIVE);
			scopes.add(spreadsheetScope);
			
			//String clientId = secrets.get("client_id").toString();
			String clientEmail = "192057351301-aiq5r3jtudblon9qblaeosajbu673efe@developer.gserviceaccount.com";//secrets.get("client_email").toString();
		
			
			
			Credential credential;
			
			File p12File = null;
			
			p12File = new File(p12Paths);
		
			
			credential = new GoogleCredential.Builder()
			.setTransport(transport)
					.setJsonFactory(factory)
					.setServiceAccountId(clientEmail)
					.setServiceAccountScopes(scopes)
					.setServiceAccountUser(username)
					.setServiceAccountPrivateKeyFromP12File(p12File)
					.build();
			
			spreadsheetService.setOAuth2Credentials(credential);
			String clientSecret = "O3wH_IMMhGNEt-U7VQ2oCZzJ";
			String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
			URL tokenRequestUrl = new URL("https://www.googleapis.com/oauth2/v3/token");
			HttpURLConnection conn = (HttpURLConnection)tokenRequestUrl.openConnection();
			conn.setRequestMethod("POST");
			String codePart = "code=4/bSfZhGihxuQeOyeotyaSJjIHTn3l1ZzaiFIx4hTJLxk&";
			String clientIdPart = "client_id=192057351301-ofvbu2pqf6fg08nbnqki8431c4i0uki3.apps.googleusercontent.com&";
			String clientSecretPart = "client_secret="+clientSecret+"&";
			String redirectUri = null;// come cazzo si trova questa merda di redirect uri
			
			/*conn.setRequestProperty("code", "4/bSfZhGihxuQeOyeotyaSJjIHTn3l1ZzaiFIx4hTJLxk&");
			conn.setRequestProperty("client_id", "192057351301-ofvbu2pqf6fg08nbnqki8431c4i0uki3.apps.googleusercontent.com&");*/
			
			spreadsheetService.setAuthSubToken("ya29.ugHaFRAKe-n5wdQJgzQ0SBTyzl0UIx0RhB55O39QMlSFJke3mhMRoBkK_Fqk7PSf7nza");
			
			URL spreadsheetFeedUrl = new URL(
			        "https://spreadsheets.google.com/feeds/spreadsheets/private/full");//*/

	   
		/*Drive drive = new Drive.Builder(transport,factory,credential).setApplicationName("mkorsds").build();
		//com.google.api.services.drive.model.File foundFile = drive.files().get(spreadsheetId).execute();
		FileList filelist = drive.files().list().execute();
		List<com.google.api.services.drive.model.File> filesListAsList = filelist.getItems();
		
		com.google.api.services.drive.model.File foundFile = drive.files().get(spreadsheetId).execute();
		//out.println(foundFile.getTitle());*/
		SpreadsheetFeed feed = spreadsheetService.getFeed(spreadsheetFeedUrl, SpreadsheetFeed.class);
	
		List <SpreadsheetEntry> entries = feed.getEntries();
		SpreadsheetEntry found = null;
		for(SpreadsheetEntry entry:entries)
		{
			String entryId = entry.getKey();
			if (entryId.equals(spreadsheetId))
			{
				found = entry;
				break;
			}
		}
	
		WorksheetEntry asInfoSheet = null;
		WorksheetEntry asSheet = null;
		if (found != null)
		{
			List <WorksheetEntry> worksheets = found.getWorksheets();
		    //assumo che il foglio di info sia il primo e il foglio di dati sia il secondo
			asInfoSheet = worksheets.get(0);
			asSheet = worksheets.get(1);
			URL cellsAsInfoSheetURL = asInfoSheet.getCellFeedUrl();
			CellFeed cellFeed = spreadsheetService.getFeed(cellsAsInfoSheetURL, CellFeed.class);
			List<CellEntry> cellEntries = cellFeed.getEntries();
			String salesOrg = null;
			String division = null;
			String seasonYear = null;
			for (CellEntry cellEntry:cellEntries)
			{
				Cell cell=cellEntry.getCell();
				int row=cell.getRow();
				int column = cell.getCol();
				//se non erro gli indici partono da 1
				if (column == 2)
				{
					if (row == 1)
					{
						salesOrg = cell.getValue();
					}
					if (row == 2)
					{
						division = cell.getValue();
					}
					if (row == 3)
					{
						seasonYear = cell.getValue();
					}
					
					if (salesOrg != null)
					{
						if (division !=null)
						{
							if (seasonYear != null)
							{
								//ho gi� trovato quello che serviva
								break;
							}
						}
					}
				}
			}
			out.println("sales org "+salesOrg);
			out.println("division "+division);
			out.println("seasonYear "+seasonYear);
			out.flush();
		}else
		{
			throw new FileNotFoundException(spreadsheetId+" not found");
		}
		/*for (com.google.api.services.drive.model.File singleFile:filesListAsList)
		{
			
			out.println(singleFile.getId());
		}
		out.flush();
		out.close();*/
			    // Make a request to the API and get all spreadsheets.
			    /*SpreadsheetFeed feed = spreadsheetService.getFeed(spreadsheetFeedUrl, SpreadsheetFeed.class);
			    List<SpreadsheetEntry> spreadsheets = feed.getEntries();
			    SpreadsheetEntry foundEntry = null;
			    for (SpreadsheetEntry spreadsheet:spreadsheets)
			    {
			    	String currentId = spreadsheet.getId();
			    	if (currentId.equals(spreadsheetId))
			    	{
			    		foundEntry = spreadsheet;
			    		break;
			    	}
			    }*/
		//System.out.println(foundFile.getId());
			
		} catch (Exception e)
		{
			throw new ServletException(e);
		}
		
	}

}
