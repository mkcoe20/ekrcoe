/**
 * 
 */
package it.ekr.ekrcoe.operations;

import it.ekr.ekrcoe.jpa.CsvField;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.olingo.odata2.api.annotation.edm.EdmFunctionImport;
import org.apache.olingo.odata2.api.annotation.edm.EdmFunctionImport.HttpMethod;
import org.apache.olingo.odata2.api.annotation.edm.EdmFunctionImport.ReturnType;
import org.apache.olingo.odata2.api.annotation.edm.EdmFunctionImport.ReturnType.Type;
import org.apache.olingo.odata2.api.annotation.edm.EdmFunctionImportParameter;
import org.apache.olingo.odata2.api.annotation.edm.EdmFacets;

import service.JpaEntityManagerFactory;
import service.Source;

/**
 * @author Marco Scarpa
 *
 */
public class OdataFunctions
{
   private EntityManager em;
   
   public OdataFunctions() throws NamingException, SQLException
   {
	   em = JpaEntityManagerFactory.getEntityManager();
   }
   
   @EdmFunctionImport(name = "DeleteAllDataFromFile",returnType = @ReturnType(
		    type = Type.SIMPLE, isCollection = false),httpMethod = HttpMethod.DELETE)
   public long deleteAllDataFromFile(@EdmFunctionImportParameter(name = "filePath",
           facets = @EdmFacets(nullable=false,maxLength = 1024))final String filePath, @EdmFunctionImportParameter(name = "mandt",facets = @EdmFacets(nullable=false))final String mandt )
   {
	  Query q =  em.createQuery("DELETE FROM CsvField field WHERE field.mandt=:m AND field.filePath=:f");
	  q.setParameter("m", mandt);
	  q.setParameter("f", filePath);
	  
	 long toReturn = 0;
	 EntityTransaction tx = em.getTransaction();//va tenuto l'oggetto perch� se non si usa lo stesso oggetto transazione sia su begin che su end si ha errore
	 tx.begin();
	 toReturn = q.executeUpdate();
	 tx.commit();
//	 em.flush();
//	 em.clear();
	  return toReturn;
   }
   
   
}
