package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.lang.Long;
import java.lang.String;

import javax.persistence.*;

import it.ekr.ekrcoe.jpa.ErrorData;

import java.util.Collection;

/**
 * Entity implementation class for Entity: ErrorData
 *
 */
@Entity

@IdClass(ErrorDataPK.class)
public class ErrorData implements Serializable {

	@Column(name="id")   
	@Id
	private String id;
	@Column(name="mandt",length=20)   
	@Id
	private String mandt;
	private String code;
	private String description;
	private Long index;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "parentErrorData_id", referencedColumnName = "id"),
		@JoinColumn(name = "parentErrorData_mandt", referencedColumnName = "mandt") 
	})
	private ErrorData parentErrorData;
	@OneToMany(mappedBy = "parentErrorData",orphanRemoval=true)
	private Collection<ErrorData> errorDetails;

	public ErrorData() {
		super();
	}   
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}   
	public String getMandt() {
		return this.mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}   
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public Long getIndex() {
		return this.index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}
	public ErrorData getParentErrorData() {
	    return parentErrorData;
	}
	public void setParentErrorData(ErrorData param) {
	    this.parentErrorData = param;
	}
	public Collection<ErrorData> getErrorDetails() {
	    return errorDetails;
	}
	public void setErrorDetails(Collection<ErrorData> param) {
	    this.errorDetails = param;
	}
   
}
