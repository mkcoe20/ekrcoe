package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.Column;

/**
 * ID class for entity: SkuPack
 *
 */ 
public class SkuPackPK  implements Serializable {   
   
	         
	private String salesOrg;         
	private String division;         
	private String seasonYear; 
	@Column(length=20)
	private String mandt;
	private static final long serialVersionUID = 1L;

	public SkuPackPK() {}

	

	public String getSalesOrg() {
		return this.salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	

	public String getDivision() {
		return this.division;
	}

	public void setDivision(String division) {
		this.division = division;
	}
	

	public String getSeasonYear() {
		return this.seasonYear;
	}

	public void setSeasonYear(String seasonYear) {
		this.seasonYear = seasonYear;
	}
	

	public String getMandt() {
		return this.mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}
	
   
	/*
	 * @see java.lang.Object#equals(Object)
	 */	
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof SkuPackPK)) {
			return false;
		}
		SkuPackPK other = (SkuPackPK) o;
		return true
			&& (getSalesOrg() == null ? other.getSalesOrg() == null : getSalesOrg().equals(other.getSalesOrg()))
			&& (getDivision() == null ? other.getDivision() == null : getDivision().equals(other.getDivision()))
			&& (getSeasonYear() == null ? other.getSeasonYear() == null : getSeasonYear().equals(other.getSeasonYear()))
			&& (getMandt() == null ? other.getMandt() == null : getMandt().equals(other.getMandt()));
	}
	
	/*	 
	 * @see java.lang.Object#hashCode()
	 */	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (getSalesOrg() == null ? 0 : getSalesOrg().hashCode());
		result = prime * result + (getDivision() == null ? 0 : getDivision().hashCode());
		result = prime * result + (getSeasonYear() == null ? 0 : getSeasonYear().hashCode());
		result = prime * result + (getMandt() == null ? 0 : getMandt().hashCode());
		return result;
	}
   
   
}
