package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CsvField
 *
 */
@Entity

@IdClass(CsvFieldPK.class)
public class CsvField implements Serializable {

	@Column(length=20)
	@Id
	private String mandt;
	@Id
	private long rowNumber;  
	@Column(length=1024)
	@Id
	private String filePath;   
	@Id
	private String label;
	private String value;
	private String usage;
	private static final long serialVersionUID = 1L;

	public CsvField() {
		super();
	}   
	public long getRowNumber() {
		return this.rowNumber;
	}

	public void setRowNumber(long rowNumber) {
		this.rowNumber = rowNumber;
	}   
	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}   
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}   
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}   
	public String getUsage() {
		return this.usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}
	public String getMandt()
	{
		return this.mandt;
	}
	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}
   
}
