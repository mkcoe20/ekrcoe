package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import it.ekr.ekrcoe.jpa.ErrorData;

/**
 * Entity implementation class for Entity: SkuPack
 *
 */
@Entity

@IdClass(SkuPackPK.class)
public class SkuPack implements Serializable {

	@Column(name="salesOrg") //per un bug di olingo bisogna indicare esplicitamente il nome della colonna per le relazioni 
	@Id
	private String salesOrg;
	@Column(name="division")
	@Id
	private String division;   
	@Column(name="seasonYear")
	@Id
	private String seasonYear;
	@Column(name="mandt",length=20)
	@Id
	private String mandt;
	private String filename;
	private String processingStatus;
	private Double processingPercent;
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "skuPack")
	private Collection<ClassificationProperty> classificationProperties;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;
	@OneToMany(orphanRemoval=true)
	private Collection<ErrorData> errorData;
	public SkuPack() {
		super();
	}   
	public String getSalesOrg() {
		return this.salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}   
	public String getDivision() {
		return this.division;
	}

	public void setDivision(String division) {
		this.division = division;
	}   
	public String getSeasonYear() {
		return this.seasonYear;
	}

	public void setSeasonYear(String seasonYear) {
		this.seasonYear = seasonYear;
	}   
	public String getMandt() {
		return this.mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}   
	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}   
	public String getProcessingStatus() {
		return this.processingStatus;
	}

	public void setProcessingStatus(String processingStatus) {
		this.processingStatus = processingStatus;
	}   
	public Double getProcessingPercent() {
		return this.processingPercent;
	}

	public void setProcessingPercent(Double processingPercent) {
		this.processingPercent = processingPercent;
	}
	public Collection<ClassificationProperty> getClassificationProperties() {
	    return classificationProperties;
	}
	public void setClassificationProperties(Collection<ClassificationProperty> param) {
	    this.classificationProperties = param;
	}
	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate()
	{
		return this.modificationDate;
	}
	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate)
	{
		this.modificationDate = modificationDate;
	}
	public Collection<ErrorData> getErrorData() {
	    return errorData;
	}
	public void setErrorData(Collection<ErrorData> param) {
	    this.errorData = param;
	}
	
   
}
