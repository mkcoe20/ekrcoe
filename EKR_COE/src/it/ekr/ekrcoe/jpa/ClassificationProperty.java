package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import it.ekr.ekrcoe.jpa.ErrorData;
import java.util.Collection;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: ClassificationProperty
 *
 */
@Entity

@IdClass(ClassificationPropertyPK.class)
public class ClassificationProperty implements Serializable {

	   
	@Id
	private String label;
	@Id
	private String sku;
	@Id
	private String mandt;
	private String value;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	@JoinColumns({
			
		@JoinColumn(name = "SkuPack_salesOrg", referencedColumnName = "salesOrg"),
			
		@JoinColumn(name = "SkuPack_division", referencedColumnName = "division"),
		@JoinColumn(name = "SkuPack_seasonYear", referencedColumnName = "seasonYear"),
		@JoinColumn(name = "SkuPack_mandt", referencedColumnName = "mandt") 
	})
	private SkuPack skuPack;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;
	@OneToMany(orphanRemoval=true)
	private Collection<ErrorData> errorData;
	public ClassificationProperty() {
		super();
	}   
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}   
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public SkuPack getSkuPack() {
	    return skuPack;
	}
	public void setSkuPack(SkuPack param) {
	    this.skuPack = param;
	}
	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate()
	{
		return this.modificationDate;
	}
	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate)
	{
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the mandt
	 */
	public String getMandt()
	{
		return this.mandt;
	}
	/**
	 * @param mandt the mandt to set
	 */
	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}
	public Collection<ErrorData> getErrorData() {
	    return errorData;
	}
	public void setErrorData(Collection<ErrorData> param) {
	    this.errorData = param;
	}
	
   
}
