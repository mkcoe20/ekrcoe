package it.ekr.ekrcoe.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;

public class ClassificationPropertyPK implements Serializable
{

	private String label;
	
	private String sku;
	
	@Column(length=20) 
	private String mandt;
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * @return the label
	 */
	public String getLabel()
	{
		return this.label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label)
	{
		this.label = label;
	}
	

   
	/*
	 * @see java.lang.Object#equals(Object)
	 */	
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ClassificationPropertyPK)) {
			return false;
		}
		ClassificationPropertyPK other = (ClassificationPropertyPK) o;
		return true
			&& (getSku() == null ? other.getSku() == null : getSku().equals(other.getSku()))
			&& (getLabel() == null ? other.getLabel() == null : getLabel().equals(other.getLabel()))
			&& (getMandt() == null ? other.getMandt() == null : getMandt().equals(other.getMandt()));
	}
	
	/*	 
	 * @see java.lang.Object#hashCode()
	 */	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (getLabel() == null ? 0 : getLabel().hashCode());
		result = prime * result + (getSku() == null ? 0 : getSku().hashCode());
		result = prime * result + (getMandt() == null ? 0 : getMandt().hashCode());
		return result;
	}

	/**
	 * @return the sku
	 */
	public String getSku()
	{
		return this.sku;
	}

	/**
	 * @param sku the sku to set
	 */
	public void setSku(String sku)
	{
		this.sku = sku;
	}

	/**
	 * @return the mandt
	 */
	public String getMandt()
	{
		return this.mandt;
	}

	/**
	 * @param mandt the mandt to set
	 */
	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}

	
	
	
}
