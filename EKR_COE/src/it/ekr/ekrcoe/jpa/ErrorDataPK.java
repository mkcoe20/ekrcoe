package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.Column;

/**
 * ID class for entity: ErrorData
 *
 */ 
public class ErrorDataPK  implements Serializable {   
   
	         
	private String id;    
	@Column(length=20) 
	private String mandt;
	private static final long serialVersionUID = 1L;

	public ErrorDataPK() {}

	

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	public String getMandt() {
		return this.mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}
	
   
	/*
	 * @see java.lang.Object#equals(Object)
	 */	
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ErrorDataPK)) {
			return false;
		}
		ErrorDataPK other = (ErrorDataPK) o;
		return true
			&& (getId() == null ? other.getId() == null : getId().equals(other.getId()))
			&& (getMandt() == null ? other.getMandt() == null : getMandt().equals(other.getMandt()));
	}
	
	/*	 
	 * @see java.lang.Object#hashCode()
	 */	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (getId() == null ? 0 : getId().hashCode());
		result = prime * result + (getMandt() == null ? 0 : getMandt().hashCode());
		return result;
	}
   
   
}
