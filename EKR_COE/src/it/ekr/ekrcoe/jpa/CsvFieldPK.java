package it.ekr.ekrcoe.jpa;

import java.io.Serializable;
import java.lang.String;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * ID class for entity: CsvField
 *
 */ 
public class CsvFieldPK  implements Serializable {   
   

	@Column(length=20)
	private String mandt;
	private long rowNumber; 
	@Column(length=1024)
	private String filePath;         
	private String label;
	private static final long serialVersionUID = 1L;

	public CsvFieldPK() {}

	

	public long getRowNumber() {
		return this.rowNumber;
	}

	public void setRowNumber(long rowNumber) {
		this.rowNumber = rowNumber;
	}
	

	public String getFilePath() {
		return this.filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
   
	/*
	 * @see java.lang.Object#equals(Object)
	 */	
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof CsvFieldPK)) {
			return false;
		}
		CsvFieldPK other = (CsvFieldPK) o;
		return true
			&& getRowNumber() == other.getRowNumber()
			&& (getFilePath() == null ? other.getFilePath() == null : getFilePath().equals(other.getFilePath()))
			&& (getLabel() == null ? other.getLabel() == null : getLabel().equals(other.getLabel()))
			&& (getMandt() == null ? other.getMandt() == null : getMandt().equals(other.getMandt()));
	}
	
	/*	 
	 * @see java.lang.Object#hashCode()
	 */	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((int) (getRowNumber() ^ (getRowNumber() >>> 32)));
		result = prime * result + (getFilePath() == null ? 0 : getFilePath().hashCode());
		result = prime * result + (getLabel() == null ? 0 : getLabel().hashCode());
		result = prime * result + (getMandt() == null ? 0 : getMandt().hashCode());
		return result;
	}



	public String getMandt()
	{
		return this.mandt;
	}



	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}
   
   
}
