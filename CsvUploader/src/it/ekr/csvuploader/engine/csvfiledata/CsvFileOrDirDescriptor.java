package it.ekr.csvuploader.engine.csvfiledata;

import java.util.Collection;

public class CsvFileOrDirDescriptor
{
	private String csvFileOrDirPath = null;
	private Boolean subdirsSearch = null;
	private Collection<String> keys = null;
	private Collection<String> extensions = null;
	private String fieldsSeparator = null;
	private String fieldsEncloser = null;
	private String mandt = null;
	private Boolean storePathEncoded = null;

	/**
	 * @return the csvFileOrDirPath
	 */
	public String getCsvFileOrDirPath()
	{
		return this.csvFileOrDirPath;
	}

	/**
	 * @param csvFileOrDirPath
	 *            the csvFileOrDirPath to set
	 */
	public void setCsvFileOrDirPath(String csvFileOrDirPath)
	{
		this.csvFileOrDirPath = csvFileOrDirPath;
	}

	

	/**
	 * @param subdirsSearch
	 *            the subdirsSearch to set
	 */
	public void setSubdirsSearch(Boolean subdirsSearch)
	{
		this.subdirsSearch = subdirsSearch;
	}

	/**
	 * @return the keys
	 */
	public Collection<String> getKeys()
	{
		return this.keys;
	}

	/**
	 * @param keys the keys to set
	 */
	public void setKeys(Collection<String> keys)
	{
		this.keys = keys;
	}

	/**
	 * @return the extensions
	 */
	public Collection<String> getExtensions()
	{
		return this.extensions;
	}

	/**
	 * @param extensions the extensions to set
	 */
	public void setExtensions(Collection<String> extensions)
	{
		this.extensions = extensions;
	}

	/**
	 * @return the subdirsSearch
	 */
	public Boolean getSubdirsSearch()
	{
		return this.subdirsSearch;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		CsvFileOrDirDescriptor clone = new CsvFileOrDirDescriptor();
		clone.setCsvFileOrDirPath(getCsvFileOrDirPath());
		clone.setExtensions(getExtensions());
		clone.setKeys(getKeys());
		clone.setSubdirsSearch(getSubdirsSearch());
		clone.setFieldsEncloser(getFieldsEncloser());
		clone.setFieldsSeparator(getFieldsSeparator());
		clone.setMandt(getMandt());
		clone.setStorePathEncoded(getStorePathEncoded());
		
		return clone;
	}

	/**
	 * @return the fieldsSeparator
	 */
	public String getFieldsSeparator()
	{
		return this.fieldsSeparator;
	}

	/**
	 * @param fieldsSeparator the fieldsSeparator to set
	 */
	public void setFieldsSeparator(String fieldsSeparator)
	{
		this.fieldsSeparator = fieldsSeparator;
	}

	/**
	 * @return the fieldsEncloser
	 */
	public String getFieldsEncloser()
	{
		return this.fieldsEncloser;
	}

	/**
	 * @param fieldsEncloser the fieldsEncloser to set
	 */
	public void setFieldsEncloser(String fieldsEncloser)
	{
		this.fieldsEncloser = fieldsEncloser;
	}

	/**
	 * @return the mandt
	 */
	public String getMandt()
	{
		return this.mandt;
	}

	/**
	 * @param mandt the mandt to set
	 */
	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}

	
	public boolean checkIfKey(String evaluating)
	{
		if (keys != null)
		{
			for (String keyString:keys)
			{
				if (keyString.equals(evaluating))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return the storePathEncoded
	 */
	public Boolean getStorePathEncoded()
	{
		return this.storePathEncoded;
	}

	/**
	 * @param storePathEncoded the storePathEncoded to set
	 */
	public void setStorePathEncoded(Boolean storePathEncoded)
	{
		this.storePathEncoded = storePathEncoded;
	}
	
}
