package it.ekr.csvuploader.engine.csvfiledata;

import it.ekr.csvuploader.engine.network.ToLoadCsvField;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.String;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.xml.bind.DatatypeConverter;


public class ExtendedCsvField extends ToLoadCsvField   {


	
	
	
	private Boolean storeEncodedPath;
	
	private URL url;
	private String odataServlet = null;
	private String entitySet = null;
	private String deleteMassivelyFunction = null;
	private String updateMassivelyServlet = null;
	private String deleteMassivelyServlet = null;
	private Boolean useServletToDelete = null;
	private String username = null;
	private String password = null;
	private Integer maxSingleUpload = null;

	public ExtendedCsvField() {
		super();
	}   
	
	
	/**
	 * @return the maxSingleUpload
	 */
	public Integer getMaxSingleUpload()
	{
		return this.maxSingleUpload;
	}


	/**
	 * @param maxSingleUpload the maxSingleUpload to set
	 */
	public void setMaxSingleUpload(Integer maxSingleUpload)
	{
		this.maxSingleUpload = maxSingleUpload;
	}


	public String getFilePathBase64Encoded()
	{
		return DatatypeConverter.printBase64Binary(filePath.getBytes());
	}
	
	public void setFilePathBase64Encoded(String filePathBase64Encoded)
	{
		byte[] bytes = DatatypeConverter.parseBase64Binary(filePathBase64Encoded);
		filePath = new String(bytes);
	}
	
	public String getFilePathBase64EncodedSubstitutingPipe()
	{
		String filePathBase64Encoded = getFilePathBase64Encoded();
		String filePathBase64EncodedSubstitutingPipe = filePathBase64Encoded.replace("/", "|");
		return filePathBase64EncodedSubstitutingPipe;
	}
	
	public void setFilePathBase64EncodedSubstitutingPipe(String filePathBase64EncodedSubstitutingPipe)
	{
		String filePathBase64Encoded= filePathBase64EncodedSubstitutingPipe.replace("|", "/");
		setFilePathBase64Encoded(filePathBase64Encoded);
	}
	
	public String getUrlEncodedFilePathBase64EncodedSubstitutingPipe() throws UnsupportedEncodingException
	{
		return URLEncoder.encode(getFilePathBase64EncodedSubstitutingPipe(),"UTF-8");
	}

	public void setUrlEncodedFilePathBase64EncodedSubstitutingPipe(String urlEncodedFilePathBase64EncodedSubstitutingPipe) throws UnsupportedEncodingException
	{
		setFilePathBase64EncodedSubstitutingPipe(URLDecoder.decode(urlEncodedFilePathBase64EncodedSubstitutingPipe,"UTF-8"));
	}
	
	public String getFileName()
	{
		File fileFromPath = new File(filePath);//anche se il path fosse solo un nome, creerebbe un oggetto file con riferimento locale che anche se inesistente potrei estrarne lo stesso il filename
		String fileName = fileFromPath.getName();
		return fileName;
		
	}
	
	public String getUrlEncodedFileName() throws UnsupportedEncodingException
	{
		return URLEncoder.encode(getFileName(),"UTF-8");
	}
	
	
	
	
	/**
	 * @return the storeEncodedPath
	 */
	public Boolean getStoreEncodedPath()
	{
		return this.storeEncodedPath;
	}
	/**
	 * @param storeEncodedPath the storeEncodedPath to set
	 */
	public void setStoreEncodedPath(Boolean storeEncodedPath)
	{
		this.storeEncodedPath = storeEncodedPath;
	}
	/**
	 * @return the url
	 */
	public URL getUrl()
	{
		return this.url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(URL url)
	{
		this.url = url;
	}
	/**
	 * @return the odataServlet
	 */
	public String getOdataServlet()
	{
		return this.odataServlet;
	}
	/**
	 * @param odataServlet the odataServlet to set
	 */
	public void setOdataServlet(String odataServlet)
	{
		this.odataServlet = odataServlet;
	}
	
	public URL getOdataUrl() throws MalformedURLException
	{
		if (odataServlet != null)
		{
			//rimuovo le barre in modo da metterle mentre si compone la stringa
			URL baseUrl = getUrl();
			if (baseUrl != null)
			{
				String baseUrlString = baseUrl.toString();
				if (baseUrlString.endsWith("/"))
				{
					baseUrlString = baseUrlString.substring(0, baseUrlString.length()-1);
				}
				String odataServletString = removeHeadingAndTrailingBars(odataServlet);
				
				String completeUrlString = baseUrlString+"/"+odataServletString+"/";
				URL toReturn = new URL(completeUrlString);
				return toReturn;
			}
			
			
		}
		return null;
	}
	/**
	 * @return the entitySet
	 */
	public String getEntitySet()
	{
		return this.entitySet;
	}
	/**
	 * @param entitySet the entitySet to set
	 */
	public void setEntitySet(String entitySet)
	{
		this.entitySet = entitySet;
	}
	
	public URL getEntitySetUrlWithoutFinalBar() throws MalformedURLException
	{
		if (entitySet != null)
		{
			URL odataUrl = getOdataUrl();
			if (odataUrl != null)
			{
				String odataUrlString = odataUrl.toString();
				//per costruzione so gi� che l'url ODATA finisce con /
				String entitySetString = removeHeadingAndTrailingBars(entitySet);
				String completeUrl = odataUrlString+entitySetString;
				URL toReturn = new URL(completeUrl);
				return toReturn;
			}
		}
		return null;
	}
	
	public URL getEntitySetUrl() throws MalformedURLException
	{
		URL withoutBar = getEntitySetUrlWithoutFinalBar();
		if (withoutBar != null)
		{
			String urlString = withoutBar.toString();
			String toRetString = urlString+"/";
			URL toReturn = new URL(toRetString);
			return toReturn;
		}
		return null;
	}
	/**
	 * @return the deleteMassivelyFunction
	 */
	public String getDeleteMassivelyFunction()
	{
		return this.deleteMassivelyFunction;
	}
	/**
	 * @param deleteMassivelyFunction the deleteMassivelyFunction to set
	 */
	public void setDeleteMassivelyFunction(String deleteMassivelyFunction)
	{
		this.deleteMassivelyFunction = deleteMassivelyFunction;
	}
	
	public URL getDeleteMassivelyFunctionUrl() throws MalformedURLException
	{
		if (deleteMassivelyFunction != null)
		{
			URL odataUrl = getOdataUrl();
			if (odataUrl!=null)
			{
				String odataUrlString = odataUrl.toString();//per costruzione finisce con /
				String deleteMassivelyFunctionString = removeHeadingAndTrailingBars(deleteMassivelyFunction);
				String completeUrl = odataUrlString+deleteMassivelyFunctionString;//non deve terminare per / perch� subito dopo c'� ? con i parametri
				URL toReturn = new URL(completeUrl);
				return toReturn;
			}
		}
		
		return  null;
	}
	
	public URL getDeleteMassivelyFunctionUrlWithParameters() throws MalformedURLException, UnsupportedEncodingException
	{
		URL withoutParams = getDeleteMassivelyFunctionUrl();
		if (withoutParams != null)
		{
			String withoutParamsString = withoutParams.toString();
			String fileNameUrlEncoded = getUrlEncodedFileName();
			if (getStoreEncodedPath())
			{
				//in tal caso devo cancellare usando il percorso codificato
				fileNameUrlEncoded = getUrlEncodedFilePathBase64EncodedSubstitutingPipe();
			}
	
			String paramString = "?filePath='"+fileNameUrlEncoded+"'"+"&"+"mandt='"+getMandt()+"'";
			String completeUrl = withoutParamsString+paramString;
			URL toReturn = new URL(completeUrl);
			return toReturn;
			
		}
		return null;
	}
	
	public URL getDeleteMassivelyServletUrl() throws MalformedURLException
	{
		if (deleteMassivelyServlet != null)
		{
			URL baseUrl = getUrl();
			if (baseUrl!=null) 
			{
				String baseUrlString = baseUrl.toString();
				if (baseUrlString.endsWith("/"))
				{
					baseUrlString = baseUrlString.substring(0, baseUrlString.length()-1);
				}
				String deleteMassivelyServletString = removeHeadingAndTrailingBars(deleteMassivelyServlet);
				String completeUrl = baseUrlString+"/"+deleteMassivelyServletString;//non deve finire con / per essere usata anche da xsjs
				URL toReturn = new URL(completeUrl);
				return toReturn;
			}
		}
		return null;
	}
	
	private String removeHeadingAndTrailingBars(String completeString)
	{
		String processingString = completeString;
		if (processingString.startsWith("/"))
		{
			processingString = processingString.substring(1);
		}
		if (processingString.endsWith("/"))
		{
			processingString = processingString.substring(0, processingString.length()-1);
		}
		return processingString;
	}
	/**
	 * @return the updateMassivelyServlet
	 */
	public String getUpdateMassivelyServlet()
	{
		return this.updateMassivelyServlet;
	}
	/**
	 * @param updateMassivelyServlet the updateMassivelyServlet to set
	 */
	public void setUpdateMassivelyServlet(String updateMassivelyServlet)
	{
		this.updateMassivelyServlet = updateMassivelyServlet;
	}
	
	public URL getUpdateMassivelyServletUrl() throws MalformedURLException
	{
		if (updateMassivelyServlet != null)
		{
			URL baseUrl = getUrl();
			if (baseUrl!=null) 
			{
				String baseUrlString = baseUrl.toString();
				if (baseUrlString.endsWith("/"))
				{
					baseUrlString = baseUrlString.substring(0, baseUrlString.length()-1);
				}
				String updateMassivelyServletString = removeHeadingAndTrailingBars(updateMassivelyServlet);
				String completeUrl = baseUrlString+"/"+updateMassivelyServletString;//non finisce per / secondo il mapping
				URL toReturn = new URL(completeUrl);
				return toReturn;
			}
		}
		
		return null;
	}
	
	public ToLoadCsvField getToLoadCsvField()
	{
		ToLoadCsvField reducedClone = new ToLoadCsvField();
		reducedClone.setFilePath(getFilePath());
		reducedClone.setLabel(getLabel());
		reducedClone.setMandt(getMandt());
		reducedClone.setRowNumber(getRowNumber());
		reducedClone.setUsage(getUsage());
		reducedClone.setValue(getValue());
		return reducedClone;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		ToLoadCsvField baseClone = (ToLoadCsvField) super.clone();
		ExtendedCsvField clone = new ExtendedCsvField();
		clone.populateFromClone(baseClone);
		//essendo oggetti immutabili posso usare in questo caso lo stesso riferimento, in caso di modifiche verranno riassegnati altri riferimenti
		clone.deleteMassivelyFunction = deleteMassivelyFunction;
		clone.entitySet = entitySet;
		clone.odataServlet = odataServlet;
		clone.storeEncodedPath = storeEncodedPath;
		clone.updateMassivelyServlet = updateMassivelyServlet;
		clone.url = url;
		clone.maxSingleUpload = maxSingleUpload;
		return clone;
	}


	/* (non-Javadoc)
	 * @see it.ekr.csvuploader.engine.network.ToLoadCsvField#populateFromClone(java.lang.Object)
	 */
	@Override
	public void populateFromClone(Object clone)
	{
		super.populateFromClone(clone);
		if (clone instanceof ExtendedCsvField)
		{
			ExtendedCsvField definedClone = (ExtendedCsvField)clone;
			//essendo oggetti immutabili posso usare in questo caso lo stesso riferimento, in caso di modifiche verranno riassegnati altri riferimenti
			definedClone.deleteMassivelyFunction = deleteMassivelyFunction;
			definedClone.entitySet = entitySet;
			definedClone.odataServlet = odataServlet;
			definedClone.storeEncodedPath = storeEncodedPath;
			definedClone.updateMassivelyServlet = updateMassivelyServlet;
			definedClone.url = url;
		}
	}


	/**
	 * @return the deleteMassivelyServlet
	 */
	public String getDeleteMassivelyServlet()
	{
		return this.deleteMassivelyServlet;
	}


	/**
	 * @param deleteMassivelyServlet the deleteMassivelyServlet to set
	 */
	public void setDeleteMassivelyServlet(String deleteMassivelyServlet)
	{
		this.deleteMassivelyServlet = deleteMassivelyServlet;
	}


	/**
	 * @return the useServletToDelete
	 */
	public Boolean getUseServletToDelete()
	{
		return this.useServletToDelete;
	}


	/**
	 * @param useServletToDelete the useServletToDelete to set
	 */
	public void setUseServletToDelete(Boolean useServletToDelete)
	{
		this.useServletToDelete = useServletToDelete;
	}


	/**
	 * @return the username
	 */
	public String getUsername()
	{
		return this.username;
	}


	/**
	 * @param username the username to set
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}


	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return this.password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	
	
	
   
}
