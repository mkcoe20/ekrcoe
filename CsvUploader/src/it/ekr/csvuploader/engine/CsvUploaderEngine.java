package it.ekr.csvuploader.engine;

import it.ekr.csvuploader.engine.csvfiledata.ExtendedCsvField;
import it.ekr.csvuploader.engine.csvfiledata.CsvFileOrDirDescriptor;
import it.ekr.csvuploader.engine.network.NetworkUploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.olingo.odata2.api.exception.ODataException;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class CsvUploaderEngine
{
	public CsvUploaderEngineContents startFromEngineContents(
			CsvUploaderEngineContents contents, Logger logger)
			throws CloneNotSupportedException, IOException, ODataException, JsonIOException, JsonSyntaxException, UnsupportedOperationException, URISyntaxException
	{
		Collection<CsvFileOrDirDescriptor> originalFilesList = contents
				.getFilesList();
		URL url = contents.getUrl();
		Collection<String> defaultExtensions = contents.getDefaultExtensions();
		Collection<String> defaultKeys = contents.getDefaultKeys();
		String defaultSeparator = contents.getDefaultFieldsSeparator();
		String defaultEncloser = contents.getDefaultFieldsEncloser();
		Boolean defaultSubdirsSearch = contents.getDefaultSubdirsSearch();
		String defaultMandt = contents.getDefaultMandt();
		Boolean defaultStorePathsEncoded = contents
				.getDefaultStorePathsEncoded();
		String odataServlet = contents.getOdataServlet();
		String entitySet = contents.getEntitySet();
		String deleteMassivelyFunction = contents.getDeleteMassivelyFunction();
		String updateMassivelyServlet = contents.getUpdateMassivelyServlet();
		String deleteMassivelyServlet = contents.getDeleteMassivelyServlet();
		Boolean useServletToDelete = contents.getUseServletToDelete();
		String username = contents.getUsername();
		String password = contents.getPassword();
		Integer maxSingleUpload = contents.getMaxSingleUpload();
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		logger.log(Level.INFO,messages.getString("EXPANDING_DIRECTORIES"));
		Collection<CsvFileOrDirDescriptor> expanded = expandFilesList(
				originalFilesList, defaultExtensions, defaultKeys,
				defaultSeparator, defaultEncloser, defaultSubdirsSearch,
				defaultMandt, defaultStorePathsEncoded);
		contents.setFilesList(expanded);
		logger.log(Level.INFO,messages
				.getString("ENDED_EXPANDING_DIRECTORIES"));
		logger.log(Level.INFO, messages.getString("STARTING_PROCESSING_FILES"));
		processExpandedList(expanded, logger, url, odataServlet,
				entitySet, deleteMassivelyFunction, updateMassivelyServlet,
				deleteMassivelyServlet, useServletToDelete, username, password,
				maxSingleUpload);
		logger.log(Level.INFO,messages.getString("ENDED_PROCESSING_FILES"));
		// ripristino i valori originali
		contents.setFilesList(originalFilesList);
		return contents;
	}

	private void processExpandedList(
			Collection<CsvFileOrDirDescriptor> expanded,
			Logger logger, URL url, String odataServlet,
			String entitySet, String deleteMassivelyFunction,
			String updateMassivelyServlet, String deleteMassivelyServlet,
			Boolean useServletToDelete, String username, String password,
			Integer maxSingleUpload) throws IOException, ODataException,
			CloneNotSupportedException, JsonIOException, JsonSyntaxException, UnsupportedOperationException, URISyntaxException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		int numberOfFiles = expanded.size();
		logger.log(Level.INFO,String.format(
				messages.getString("PROCESSING_N_FILES"), numberOfFiles));
		for (CsvFileOrDirDescriptor singleDescriptor : expanded)
		{
			processSingleDescriptor(singleDescriptor, logger, url,
					odataServlet, entitySet, deleteMassivelyFunction,
					updateMassivelyServlet, deleteMassivelyServlet,
					useServletToDelete, username, password, maxSingleUpload);
		}
		logger.log(Level.INFO,String.format(
				messages.getString("PROCESSED_N_FILES"), numberOfFiles));
	}

	private void processSingleDescriptor(CsvFileOrDirDescriptor descriptor,
			Logger logger, URL url, String odataServlet,
			String entitySet, String deleteMassivelyFunction,
			String updateMassivelyServlet, String deleteMassivelyServlet,
			Boolean useServletToDelete, String username, String password,
			Integer maxSingleUpload) throws IOException, ODataException,
			CloneNotSupportedException, JsonIOException, JsonSyntaxException, UnsupportedOperationException, URISyntaxException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		String path = descriptor.getCsvFileOrDirPath();
		Collection<String> keys = descriptor.getKeys();
		String keysString = costructJoinedString(keys, ",");
		String encloser = descriptor.getFieldsEncloser();
		String separator = descriptor.getFieldsSeparator();
		String mandt = descriptor.getMandt();
		Boolean storeEncodedPath = descriptor.getStorePathEncoded();
		String fileProcessingMessage = String.format(
				messages.getString("PROCESSING_FILE"), path);
		String fileProcessingMessageExpanded = String.format(
				messages.getString("PROCESSING_FILE_WITH"), path);
		AtomicBoolean messageExpanded = new AtomicBoolean(false);
		fileProcessingMessage = addPartToFileProcessingMessage(url.toString(),
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, "url:");// non traduco url
		fileProcessingMessage = addPartToFileProcessingMessage(odataServlet,
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, messages.getString("ODATA_SERVLET"));
		fileProcessingMessage = addPartToFileProcessingMessage(entitySet,
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, "entity set:");// non traduco entity set
		if (useServletToDelete == null
				|| useServletToDelete.booleanValue() == false)
		{
			fileProcessingMessage = addPartToFileProcessingMessage(
					deleteMassivelyFunction, fileProcessingMessageExpanded,
					messageExpanded, fileProcessingMessage,
					messages.getString("MASSIVE_DELETE_FUNCTION"));
		} else
		{
			fileProcessingMessage = addPartToFileProcessingMessage(
					deleteMassivelyServlet, fileProcessingMessageExpanded,
					messageExpanded, fileProcessingMessage,
					"servlet cancellazione massiva:");
		}

		fileProcessingMessage = addPartToFileProcessingMessage(
				updateMassivelyServlet, fileProcessingMessageExpanded,
				messageExpanded, fileProcessingMessage,
				messages.getString("MASSIVE_LOAD_SERVLET"));
		fileProcessingMessage = addPartToFileProcessingMessage(mandt,
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, "mandt:");// non traduco mandt
		fileProcessingMessage = addPartToFileProcessingMessage(keysString,
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, messages.getString("KEYS"));
		fileProcessingMessage = addPartToFileProcessingMessage(separator,
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, messages.getString("SEPARATOR_IN_FILE"));
		fileProcessingMessage = addPartToFileProcessingMessage(encloser,
				fileProcessingMessageExpanded, messageExpanded,
				fileProcessingMessage, messages.getString("QUOTE_CHARACTER"));
		String maxSingleUploadString = null;
		if (maxSingleUpload != null)
		{
			maxSingleUploadString = maxSingleUpload.toString();
		}
		fileProcessingMessage = addPartToFileProcessingMessage(
				maxSingleUploadString, fileProcessingMessageExpanded,
				messageExpanded, fileProcessingMessage,
				"massimo campi singolo caricamento:");
		logger.log(Level.INFO,fileProcessingMessage);
		CSVFormat format = CSVFormat.DEFAULT;
		if (separator != null)
		{
			format = format.withDelimiter(separator.charAt(0));
		}
		if (encloser != null)
		{
			format = format.withQuote(encloser.charAt(0));
		}
		File csvFile = new File(path);
		FileInputStream inputStream = new FileInputStream(csvFile);
		BOMInputStream bomInput = new BOMInputStream(inputStream);
		InputStreamReader reader = new InputStreamReader(bomInput);
		CSVParser parser = format.parse(reader);

		List<CSVRecord> records = parser.getRecords();
		CSVRecord headerRecord = records.get(0);

		long linesCounter = 0;
		List<ExtendedCsvField> fields = new ArrayList<ExtendedCsvField>();
		for (CSVRecord record : records)
		{
			logger.log(Level.INFO,String.format(
					messages.getString("PROCESSING_ROW_FILE"), linesCounter,
					path));
			for (int i = 0; i < record.size(); i++)
			{
				String headString = headerRecord.get(i);
				ExtendedCsvField field = new ExtendedCsvField();
				String value = record.get(i);
				boolean isKey = descriptor.checkIfKey(headString);
				boolean isKeyLabel = false;
				boolean isFieldLabel = false;
				if (linesCounter == 0)
				{
					if (isKey)
					{
						// se � stata classificata come chiave in realt� non �
						// chiave ma una delle etichette della chiave
						isKeyLabel = true;
					} else
					{
						isFieldLabel = true;
					}
					isKey = false;// la riga di header non contiene chiavi, se
									// no c'� il rischio che venga trovata come
									// chiave una etichetta dell' header
				} else
				{
					isFieldLabel = false;
					isKeyLabel = false;// nelle righe che non sono intestazione
										// non ci possono essere etichette di
										// chiave ne etichette di campi
				}
				if (isKey)
				{
					field.setUsage("KEY");
				} else if (isKeyLabel)
				{
					field.setUsage("KEY_LABEL");
				} else if (isFieldLabel)
				{
					field.setUsage("FIELD_LABEL");
				} else
				{
					field.setUsage("FIELD");
				}
				field.setFilePath(path);
				field.setMandt(mandt);
				field.setLabel(headString);
				field.setRowNumber(linesCounter);
				field.setValue(value);
				field.setStoreEncodedPath(storeEncodedPath);
				field.setUrl(url);
				field.setOdataServlet(odataServlet);
				field.setEntitySet(entitySet);
				field.setDeleteMassivelyFunction(deleteMassivelyFunction);
				field.setUpdateMassivelyServlet(updateMassivelyServlet);
				field.setDeleteMassivelyServlet(deleteMassivelyServlet);
				field.setUseServletToDelete(useServletToDelete);
				field.setUsername(username);
				field.setPassword(password);
				field.setMaxSingleUpload(maxSingleUpload);
				// non devo caricare campi vuoti, a meno che non siano chiave
				if (!value.equals(""))
				{
					fields.add(field);
				} else
				{
					if (isKey)
					{
						fields.add(field);
					}
				}

			}
			logger.log(Level.INFO,String.format(
					messages.getString("ENDED_PROCESSING_ROW_FILE"),
					linesCounter, path));
			linesCounter++;
		}
		logger.log(Level.INFO,String.format(messages
				.getString("STARTING_UPLOAD_OPERATIOND_FILE_WITH_MANDT"), path,
				mandt));
		NetworkUploader networkUploader = new NetworkUploader();
		networkUploader.uploadCsvFields(fields, true, logger);
		logger.log(Level.INFO,String.format(
				messages.getString("ENDED_UPLOAD_OPERATIOND_FILE_WITH_MANDT"),
				path, mandt));
		logger.log(Level.INFO,String.format(
				messages.getString("ENDED_PROCESSING_FILE"), path));
	}

	/**
	 * @param partString
	 * @param fileProcessingMessageExpanded
	 * @param messageExpanded
	 */
	private String addPartToFileProcessingMessage(String partString,
			String fileProcessingMessageExpanded,
			AtomicBoolean messageExpanded, String fileProcessingMessage,
			String partDescription)
	{
		if (partString != null)
		{
			if (messageExpanded.get() == false)
			{
				fileProcessingMessage = fileProcessingMessageExpanded;
				messageExpanded.set(true);
			}
			fileProcessingMessage = fileProcessingMessage + " "
					+ partDescription + " " + partString;
		}
		return fileProcessingMessage;
	}

	private String costructJoinedString(Collection<String> collection,
			String separator)
	{
		if (collection != null)
		{
			Iterator<String> iterator = collection.iterator();
			int size = collection.size();
			if (size == 1)
			{
				// se la collezione ha una sola stringa la restituisco
				String value = iterator.next();
				return value;

			} else if (size == 0)
			{
				return "";// se ha dimensione 0 restituisco stringa vuota
			} else
			{
				String costructing = "";
				if (separator == null)
				{
					separator = "";
				}
				while (iterator.hasNext())
				{
					String current = iterator.next();
					if (iterator.hasNext())// se non � l'ultima
					{
						costructing = costructing + current + separator;
					} else
					{
						costructing = costructing + current;
					}
				}
				return costructing;
			}
		}
		return null;
	}

	private Collection<CsvFileOrDirDescriptor> expandFilesList(
			Collection<CsvFileOrDirDescriptor> original,
			Collection<String> defaultExtensions,
			Collection<String> defaultKeys, String defaultSeparator,
			String defaultEncloser, Boolean defaultSubdirsSearch,
			String defaultMandt, Boolean defaultStorePathsEncoded)
			throws CloneNotSupportedException, IOException
	{
		Collection<CsvFileOrDirDescriptor> toReturn;
		if (original == null)
		{
			// parto dalla directory corrente
			File currentDir = new File(".");
			toReturn = expandDirectory(currentDir, defaultExtensions,
					defaultKeys, defaultSeparator, defaultEncloser,
					defaultSubdirsSearch, defaultMandt,
					defaultStorePathsEncoded);
		} else
		{
			toReturn = new ArrayList<CsvFileOrDirDescriptor>();
			for (CsvFileOrDirDescriptor singleOriginal : original)
			{
				CsvFileOrDirDescriptor explicitatedDefaults = (CsvFileOrDirDescriptor) singleOriginal
						.clone();
				if (explicitatedDefaults.getSubdirsSearch() == null)
				{
					explicitatedDefaults.setSubdirsSearch(defaultSubdirsSearch);
				}
				if (explicitatedDefaults.getExtensions() == null)
				{
					explicitatedDefaults.setExtensions(defaultExtensions);
				}
				if (explicitatedDefaults.getKeys() == null)
				{
					explicitatedDefaults.setKeys(defaultKeys);
				}
				if (explicitatedDefaults.getFieldsEncloser() == null)
				{
					explicitatedDefaults.setFieldsEncloser(defaultEncloser);
				}
				if (explicitatedDefaults.getFieldsSeparator() == null)
				{
					explicitatedDefaults.setFieldsSeparator(defaultSeparator);
				}
				if (explicitatedDefaults.getMandt() == null)
				{
					explicitatedDefaults.setMandt(defaultMandt);
				}
				if (explicitatedDefaults.getStorePathEncoded() == null)
				{
					explicitatedDefaults
							.setStorePathEncoded(defaultStorePathsEncoded);
				}
				String pathString = explicitatedDefaults.getCsvFileOrDirPath();
				File checkIfDir = new File(pathString);
				if (checkIfDir.exists())
				{
					if (checkIfDir.isDirectory())
					{
						// devo comunque espanderla perch� � la directory data
						// nel file di configurazione, poi eventualmente non
						// espande le sottodirectory
						Collection<CsvFileOrDirDescriptor> subdirExpanded = expandDirectory(
								checkIfDir,
								explicitatedDefaults.getExtensions(),
								explicitatedDefaults.getKeys(),
								explicitatedDefaults.getFieldsSeparator(),
								explicitatedDefaults.getFieldsEncloser(),
								explicitatedDefaults.getSubdirsSearch(),
								explicitatedDefaults.getMandt(),
								explicitatedDefaults.getStorePathEncoded());
						toReturn.addAll(subdirExpanded);

					} else
					{
						toReturn.add(explicitatedDefaults);
					}
				} else
				{
					ResourceBundle messages = ResourceBundle.getBundle(
							"messages", Locale.getDefault());
					throw new FileNotFoundException(String.format(
							messages.getString("FILE_NOT_EXISTS"),
							checkIfDir.getAbsolutePath()));
				}
			}
		}

		return toReturn;
	}

	private Collection<CsvFileOrDirDescriptor> expandDirectory(File directory,
			Collection<String> extensions, Collection<String> keys,
			String separator, String encloser, Boolean recoursive,
			String mandt, Boolean storePathsEncoded) throws IOException
	{
		File[] contents = directory.listFiles();
		List<CsvFileOrDirDescriptor> toReturn = new ArrayList<CsvFileOrDirDescriptor>();
		if (recoursive == null)
		{
			recoursive = new Boolean(false);// se non � definito, di default non
											// espande
		}
		if (storePathsEncoded == null)
		{
			storePathsEncoded = false;
		}
		for (File singleContent : contents)
		{
			if (singleContent.isDirectory())
			{
				if (recoursive)
				{
					toReturn.addAll(expandDirectory(singleContent, extensions,
							keys, separator, encloser, recoursive, mandt,
							storePathsEncoded));
				}
			} else
			{
				if (extensions == null)
				{
					// se non ho estensioni specificate inserisco comunque il
					// file
					insertSingleFileInExpandedList(singleContent, keys,
							separator, encloser, toReturn, mandt,
							storePathsEncoded);
				} else
				{
					String fileExtension = FilenameUtils
							.getExtension(singleContent.getAbsolutePath());
					for (String extension : extensions)
					{
						if (fileExtension.equalsIgnoreCase(extension))
						{
							insertSingleFileInExpandedList(singleContent, keys,
									separator, encloser, toReturn, mandt,
									storePathsEncoded);
							break;
						}
					}
				}
			}
		}
		return toReturn;
	}

	/**
	 * @param singleContent
	 * @param keys
	 * @param toReturn
	 * @throws IOException
	 */
	private void insertSingleFileInExpandedList(File singleContent,
			Collection<String> keys, String separator, String encloser,
			List<CsvFileOrDirDescriptor> toReturn, String mandt,
			Boolean storePathEncoded) throws IOException
	{
		CsvFileOrDirDescriptor currentExpandedFileDescriptor = new CsvFileOrDirDescriptor();
		currentExpandedFileDescriptor.setCsvFileOrDirPath(singleContent
				.getCanonicalPath());
		currentExpandedFileDescriptor.setKeys(keys);
		currentExpandedFileDescriptor.setFieldsSeparator(separator);
		currentExpandedFileDescriptor.setFieldsEncloser(encloser);
		currentExpandedFileDescriptor.setMandt(mandt);
		currentExpandedFileDescriptor.setStorePathEncoded(storePathEncoded);
		toReturn.add(currentExpandedFileDescriptor);
	}
}
