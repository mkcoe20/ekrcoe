package it.ekr.csvuploader.engine.network;

public class Cookie
{
	public Cookie(String description)
	{
		String [] splitted = description.split("; ");
		int pos = 0;
		for (String singleCookieSplit:splitted)
		{
			if (singleCookieSplit.contains("="))
			{
				String [] subSplit = singleCookieSplit.split("=");
				String splittedKey = subSplit[0];
				String splittedValue = subSplit[1];
				if ("path".equals(splittedKey))
				{
					path = splittedValue;
				}else
				{
					if (pos == 0)
					{
						key = splittedKey;
						value = splittedValue;
					}
					
				}
			}else if (singleCookieSplit.equals("HttpOnly"))
			{
				httpOnly = true;
			}
			
			pos ++;
		}
	}

	private String key = null;
	private String value = null;
	private String path = null;
	private boolean httpOnly = false;

	/**
	 * @return the key
	 */
	public String getKey()
	{
		return this.key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key)
	{
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue()
	{
		return this.value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

	/**
	 * @return the path
	 */
	public String getPath()
	{
		return this.path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path)
	{
		this.path = path;
	}

	/**
	 * @return the httpOnly
	 */
	public boolean isHttpOnly()
	{
		return this.httpOnly;
	}

	/**
	 * @param httpOnly
	 *            the httpOnly to set
	 */
	public void setHttpOnly(boolean httpOnly)
	{
		this.httpOnly = httpOnly;
	}
	
	public String getKeyValueCoupled()
	{
		if ((key != null)&&(value != null))
		{
			return key+"="+value;
		}
		return null;
	}

}
