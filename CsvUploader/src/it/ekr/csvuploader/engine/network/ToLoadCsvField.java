package it.ekr.csvuploader.engine.network;

import java.io.Serializable;

public class ToLoadCsvField implements Serializable
{
	public ToLoadCsvField()
	{
		super();
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String mandt;

	protected long rowNumber;  

	protected String filePath;   

	protected String label;
	protected String value;
	protected String usage;
	
	public long getRowNumber() 
	{
		return this.rowNumber;
	}

	public void setRowNumber(long rowNumber) 
	{
		this.rowNumber = rowNumber;
	}   
	public String getFilePath() 
	{
		return this.filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}   
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}   
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}   
	public String getUsage() {
		return this.usage;
	}

	public void setUsage(String usage) {
		this.usage = usage;
	}
	public String getMandt()
	{
		return this.mandt;
	}
	public void setMandt(String mandt)
	{
		this.mandt = mandt;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		ToLoadCsvField clone = new ToLoadCsvField();
		clone.setFilePath(getFilePath());
		clone.setLabel(getLabel());
		clone.setMandt(getMandt());
		clone.setRowNumber(getRowNumber());
		clone.setUsage(getUsage());
		clone.setValue(getValue());
		return clone;
	}
	
	public void populateFromClone(Object clone)
	{
		if (clone instanceof ToLoadCsvField)
		{
			ToLoadCsvField definedClone = (ToLoadCsvField) clone;
			setFilePath(definedClone.getFilePath());
			setLabel(definedClone.getLabel());
			setMandt(definedClone.getMandt());
			setRowNumber(definedClone.getRowNumber());
			setUsage(definedClone.getUsage());
			setValue(definedClone.getValue());
		}
		
		
	}
	
	
}
