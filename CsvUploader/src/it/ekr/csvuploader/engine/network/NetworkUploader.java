package it.ekr.csvuploader.engine.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FilePermission;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.input.ReaderInputStream;
import org.apache.olingo.odata2.api.commons.HttpStatusCodes;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.edm.EdmEntityContainer;
import org.apache.olingo.odata2.api.edm.EdmException;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderException;
import org.apache.olingo.odata2.api.ep.EntityProviderReadProperties;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.api.exception.ODataException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import it.ekr.csvuploader.engine.csvfiledata.ExtendedCsvField;

public class NetworkUploader
{
	public static final int REDIRECT_SAP_REQUEST = 302;
	public static final String HTTP_METHOD_PUT = "PUT";
	public static final String HTTP_METHOD_POST = "POST";
	public static final String HTTP_METHOD_GET = "GET";
	private static final String HTTP_METHOD_DELETE = "DELETE";

	public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HTTP_HEADER_ACCEPT = "Accept";

	public static final String APPLICATION_JSON = "application/json";
	public static final String APPLICATION_XML = "application/xml";
	public static final String ACCEPT_CONTENT_FOR_SESSION_CLOSING = "*/*";
	public static final String METADATA = "$metadata";
	
	private URL firstConnectionUrl = null;
	
	private URL redirectionUrl = null;
	
	private List<Cookie> cookies = null;
	
	private List<Cookie> calculateCookies(List <String> httpRetrievedCookies)
	{
		if (httpRetrievedCookies != null)
		{
			List<Cookie> toReturn = new ArrayList<Cookie>();
			for (String cookieString:httpRetrievedCookies)
			{
				Cookie buildingCookie = new Cookie(cookieString);
				toReturn.add(buildingCookie);
			}
			cookies = toReturn;
			return cookies;
		}else
		{
			cookies = null;
		}
		return null;
	}
	
	private boolean isIntoSapSession()
	{
		if (cookies != null)
		{
			for (Cookie cookie:cookies)
			{
				if ("sapxslb".equals(cookie.getKey()))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public void cleanupCookies()
	{
		cookies = null;
	}
	
	public void closeSapSession(String username,String password) throws IOException
	{
		if (isIntoSapSession())
		{
			URL baseUrlForClosingSession = firstConnectionUrl;
			if (redirectionUrl != null)
			{
				//se sono stato redirezionato uso l'ultimo url a cui sono stato redirezionato come base
				baseUrlForClosingSession = redirectionUrl;
			}
			String protocol = baseUrlForClosingSession.getProtocol();
			String host = baseUrlForClosingSession.getHost();
			int port = baseUrlForClosingSession.getPort();
			String file = "/sap/hana/xs/formLogin/token.xsjs";
			URL tokenRequestUrl = new URL(protocol, host, port, file);
			
			HttpURLConnection tokenRequestConnection = initializeConnection(tokenRequestUrl.toString(), ACCEPT_CONTENT_FOR_SESSION_CLOSING, HTTP_METHOD_GET, username, password);
			tokenRequestConnection.setRequestProperty("X-CSRF-Token", "Fetch");
			tokenRequestConnection.setRequestProperty("Cookie", calculateCookiesStringToSend());
			tokenRequestConnection.connect();
			Map<String, List<String>> headers = tokenRequestConnection
					.getHeaderFields();
			Set<String> headerKeys = headers.keySet();
			List<String> httpRetrievedToken = null;
			for (String headerKey:headerKeys)
			{
			   if ("x-csrf-token".equalsIgnoreCase(headerKey))
			   {
				   httpRetrievedToken = headers.get(headerKey);
			   }
			}
			tokenRequestConnection.disconnect();
			String token = httpRetrievedToken.get(0);
			if (!("unsafe".equals(token)))
			{
				//se la sessione esiste ancora abbinata ai cookies spediti
				//chiudo la sessione con la post
				file = "/sap/hana/xs/formLogin/logout.xscfunc";
				URL closingSessionUrl = new URL(protocol, host, port, file);
				HttpURLConnection closingSessionConnection = initializeConnection(closingSessionUrl.toString(), APPLICATION_JSON, HTTP_METHOD_POST, username, password);
				closingSessionConnection.setRequestProperty("X-CSRF-Token", token);
				closingSessionConnection.setRequestProperty("Cookie", calculateCookiesStringToSend());
				OutputStream outputStream = closingSessionConnection.getOutputStream();
				outputStream.flush();
				outputStream.close();
				closingSessionConnection.connect();
				InputStream inputStream = closingSessionConnection.getInputStream();
				inputStream.close();
				closingSessionConnection.disconnect();
			}
			
		}
		cookies = null;
		
	}
	
	public String calculateCookiesStringToSend()
	{
		if (cookies != null)
		{
			String toReturn = "";
			int cookiesListSize = cookies.size();
			if (cookiesListSize > 0)
			{
				Cookie first = cookies.get(0);
				toReturn += first.getKeyValueCoupled();
				for (int i=1;i<cookiesListSize;i++)// parto dal secondo e metto il ; in testa
				{
					toReturn += ";";
					Cookie cookie = cookies.get(i);
					toReturn += cookie.getKeyValueCoupled();
				}
			}
			return toReturn;
		}
		return null;
	}

	public Edm readEdm(String serviceUrl,String username,String password) throws IOException, ODataException, URISyntaxException
	{
		InputStream content = execute(serviceUrl + METADATA, APPLICATION_XML,
				HTTP_METHOD_GET,username,password);
		return EntityProvider.readMetadata(content, false);
	}

	public void uploadCsvFields(List<ExtendedCsvField> list,
			boolean deleteOldContents, Logger logger)
			throws IOException, ODataException, CloneNotSupportedException, JsonIOException, JsonSyntaxException, UnsupportedOperationException, URISyntaxException
	{

		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		Edm edm = null;
		// assumo che potrebbero esserci dati provenienti da files diversi
		List<List<ExtendedCsvField>> dividedByFile = divideByFileAndMandt(list);
		for (List<ExtendedCsvField> singleFileList : dividedByFile)
		{
			ExtendedCsvField first = singleFileList.get(0);
			if (first.getUseServletToDelete() == null
					|| first.getUseServletToDelete().booleanValue() == false)
			{
				// l'oggetto edm non viene usato se non dagli odata, se uso la
				// servlet per caricare e per cancellare non serve edm e potrei
				// avere errori visto che in hana xsjs non si trova
				if (edm == null)
				{
					edm = readEdm(first.getOdataUrl().toString(),first.getUsername(),first.getPassword());
				}
			}

			String filePathOrName = first.getFilePath();
			if (first.getStoreEncodedPath().booleanValue() == false)
			{
				filePathOrName = first.getFileName();
			}
			if (deleteOldContents)// ad ogni nuovo caricamento i dati vecchi
									// andrebbero cancellati, ma se la lista
									// viene creata in modi diversi, potrebbe
									// servire gestire la cancellazione in
									// maniera personalizzata
			{

				logger.log(Level.INFO, String.format(
								messages.getString("DELETING_PREVIOUS_UPLOADED_DATA_FOR_FILE_WITH_MANDT"),
								filePathOrName, first.getMandt()));
				deleteFileData(first, logger, edm);
				logger.log(Level.INFO,String.format(
								messages.getString("ENDED_DELETING_PREVIOUS_UPLOADED_DATA_FOR_FILE_WITH_MANDT"),
								filePathOrName, first.getMandt()));
			}
			logger.log(Level.INFO, String.format(
					messages.getString("UPLOAD_FILE_DATA_WITH_MANDT"),
					filePathOrName, first.getMandt()));
			uploadSingleFileData(singleFileList, logger, edm);
			closeSapSession(first.getUsername(),first.getPassword());
			logger.log(Level.INFO, String.format(
					messages.getString("ENDED_UPLOAD_FILE_DATA_WITH_MANDT"),
					filePathOrName, first.getMandt()));
		}
	}

	public void deleteFileData(ExtendedCsvField prototypeField,
			Logger logger, Edm edm) throws IOException,
			EdmException, EntityProviderException, CloneNotSupportedException, JsonIOException, JsonSyntaxException, UnsupportedOperationException, URISyntaxException
	{
		if (prototypeField.getUseServletToDelete() == null
				|| prototypeField.getUseServletToDelete().booleanValue() == false)
		{
			deleteFileDataUsingOdataFunction(prototypeField, logger);
		} else
		{
			deleteFileDataUsingServlet(prototypeField, logger);
		}

	}

	/**
	 * funzione per cancellare massivamente un file csv memorizzato usando la
	 * funzione odata
	 * 
	 * @param prototypeField
	 * @param feedbackStream
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException
	 * @throws UnsupportedOperationException
	 * @throws IOException
	 * @throws JsonIOException
	 * @throws JsonSyntaxException
	 * @throws URISyntaxException 
	 */
	protected void deleteFileDataUsingOdataFunction(
			ExtendedCsvField prototypeField, Logger logger)
			throws MalformedURLException, UnsupportedEncodingException,
			UnsupportedOperationException, IOException, JsonIOException,
			JsonSyntaxException, URISyntaxException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		URL deleteUrlWithParameters = prototypeField
				.getDeleteMassivelyFunctionUrlWithParameters();
		String username = prototypeField.getUsername();
		String password = prototypeField.getPassword();
		if (deleteUrlWithParameters == null)
		{
			throw new UnsupportedOperationException(
					messages.getString("SUPPORTED_ONLY_DELETE_FUNCTION"));
			// List<ExtendedCsvField> alreadyStored =
			// retrieveNetworkRecordsFromPrototype(
			// prototypeField, feedbackStream,edm);
		} else
		{
			InputStream deleteFeedback = execute(
					deleteUrlWithParameters.toString(), APPLICATION_JSON,
					HTTP_METHOD_DELETE,username,password);
			InputStreamReader reader = new InputStreamReader(deleteFeedback);
			// BufferedReader bufferedReader = new BufferedReader(reader);
			// while (bufferedReader.ready())
			// {
			// feedbackStream.println(bufferedReader.readLine());
			// }
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject) parser.parse(reader);
			JsonElement d = json.get("d");// se la risposta � corretta, dovrebbe
											// essere incapsulata in un oggetto
											// d
			if (d != null)
			{
				JsonObject dObject = (JsonObject) d;
				JsonElement responseData = dObject.get(prototypeField
						.getDeleteMassivelyFunction());// la risposta �
														// costituita da un
														// intero con lo stesso
														// nome della funzione
														// che dice quanti campi
														// sono stati cancellati
				if (responseData != null)
				{
					String deletedFields = responseData.getAsString();
					logger.log(Level.INFO, String.format(
									messages.getString("DELETED_N_FIELDS_ALREADY_STORED_FOR_FILE_WITH_MANDT"),
									deletedFields.toString(), prototypeField
											.getFileName(), prototypeField
											.getMandt()));
				}
			}
			reader.close();
		}
	}

	protected void deleteFileDataUsingServlet(ExtendedCsvField prototypeField,
			Logger logger) throws MalformedURLException,
			UnsupportedEncodingException, UnsupportedOperationException,
			IOException, JsonIOException, JsonSyntaxException, URISyntaxException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		URL deleteServletUrl = prototypeField.getDeleteMassivelyServletUrl();
		String filePathOrName = prototypeField
				.getFilePathBase64EncodedSubstitutingPipe();
		String mandt = prototypeField.getMandt();
		String username = prototypeField.getUsername();
		String password = prototypeField.getPassword();
		if (prototypeField.getStoreEncodedPath() == null
				|| prototypeField.getStoreEncodedPath().booleanValue() == false)
		{
			filePathOrName = prototypeField
					.getFileName();
		}
		if (deleteServletUrl == null)
		{
			throw new UnsupportedOperationException(
					"errore configurazione per trovare servlet per cancellazione");
			// List<ExtendedCsvField> alreadyStored =
			// retrieveNetworkRecordsFromPrototype(
			// prototypeField, feedbackStream,edm);
		} else
		{
			HttpURLConnection conn = initializeConnection(
					deleteServletUrl.toString(), APPLICATION_JSON,
					HTTP_METHOD_DELETE,username,password);
			
			

			
			JsonObject outputJson = new JsonObject();
			outputJson.addProperty("filePath", filePathOrName);
			outputJson.addProperty("mandt", mandt);
			String jsonToUpload = outputJson.toString();
			conn.setRequestProperty("Content-Length",
					"" + Integer.toString(jsonToUpload.getBytes().length));
			if (isIntoSapSession())
			{
				conn.setRequestProperty("Cookie", calculateCookiesStringToSend());
			}
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(jsonToUpload);
			wr.flush();
			wr.close();
			Map<String, List<String>> headers = conn
					.getHeaderFields();
			Set<String> headerKeys = headers.keySet();
			List<String> httpRetrievedCookies = null;
			for (String headerKey:headerKeys)
			{
			   if ("set-cookie".equalsIgnoreCase(headerKey))//sap usa set-cookie e non set-cookie2 che consentirebbe di gestirlo con un cookie handler
			   {
				   httpRetrievedCookies = headers.get(headerKey);
			   }
			}
			if (!isIntoSapSession())
			{
				calculateCookies(httpRetrievedCookies);
			}
			
			
			InputStream responseStream = conn.getInputStream();// bisogna
			// avere
			// attivi
			// entrambi
			// gli
			// stream,
			// se no
			// non
			// va il
			// caricamento
			InputStreamReader reader = new InputStreamReader(responseStream);
			JsonParser parser = new JsonParser();
			JsonObject inputJson = (JsonObject) parser.parse(reader);
			JsonElement message = inputJson.get("message");
			JsonElement filePathFeedback = inputJson.get("filePath");
			if (message != null)
			{
				String  messageResponse = message.getAsString();
				logger.log(Level.INFO, "risultato elaborazione: "+messageResponse);//TODO localizzare
				if (filePathFeedback != null)
				{
					String  filePathResponse = filePathFeedback.getAsString();
					logger.log(Level.INFO, "sul file: "+filePathResponse);//TODO localizzare
				}
			}
			conn.disconnect();
			// verifico se mi ha mandato una redirect, in tal caso devo ricreare
			// la connessione
			HttpURLConnection remade = eventuallyRemakeConnection(conn,
					APPLICATION_JSON, HTTP_METHOD_DELETE,username,password, false);
			if (remade != conn)
			{
				// vuol dire che ho avuto un redirect, e quindi devo rifare
				conn = remade;
				wr = new DataOutputStream(conn.getOutputStream());
				wr.writeBytes(jsonToUpload);
				wr.flush();
				wr.close();
				responseStream = conn.getInputStream();// bisogna
				// avere
				// attivi
				// entrambi
				// gli
				// stream,
				// se
				// no
				// non
				// va
				// il
				// caricamento
				reader = new InputStreamReader(responseStream);
				parser = new JsonParser();
				inputJson = (JsonObject) parser.parse(reader);
				message = inputJson.get("message");
				filePathFeedback = inputJson.get("filePath");
				if (message != null)
				{
					String  messageResponse = message.getAsString();
					logger.log(Level.INFO, "risultato elaborazione: "+messageResponse);//TODO localizzare
					if (filePathFeedback != null)
					{
						String  filePathResponse = filePathFeedback.getAsString();
						logger.log(Level.INFO,"sul file: "+filePathResponse);//TODO localizzare
					}
				}
				conn.disconnect();
			}
			/*
			 * OutputStream uploadStream = connection.getOutputStream();
			 * PrintWriter writer = new PrintWriter(uploadStream);
			 * writer.print(jsonToUpload); writer.flush(); writer.close();
			 */

		}
	}

	/*
	 * private List<ExtendedCsvField> retrieveNetworkRecordsFromPrototype(
	 * ExtendedCsvField prototypeField, PrintStream feedbackStream, Edm edm)
	 * throws IOException, EdmException, EntityProviderException { String path =
	 * prototypeField.getFilePath(); if
	 * (prototypeField.getStoreEncodedPath().booleanValue() == false) { path =
	 * prototypeField.getFileName(); } String mandt = prototypeField.getMandt();
	 * List<ExtendedCsvField> found = new ArrayList<ExtendedCsvField>();
	 * feedbackStream.println("ricerca valori gi� caricati per file " + path +
	 * " mandt " + mandt); System.out.println("encoding del path " +
	 * prototypeField.getFileName() + " -> " + prototypeField
	 * .getUrlEncodedFilePathBase64EncodedSubstitutingPipe()); // inserire
	 * istruzioni di ricerca //
	 * https://provamscaricamep1940761120tria.hanatrial.
	 * ondemand.com:443/provaMsCaricamentoCsv/odata/CsvFieldSet/?$filter=mandt
	 * // eq 'EKR' and filePath eq '%2Bprova.csv%7C%3D' URL baseUrl =
	 * prototypeField.getUrl(); baseUrl =
	 * createEntitySetUrlFromGivenUrl(baseUrl); String urlEncodedPath =
	 * prototypeField .getUrlEncodedFilePathBase64EncodedSubstitutingPipe(); if
	 * (prototypeField.getStoreEncodedPath() == false) { urlEncodedPath =
	 * prototypeField.getUrlEncodedFileName(); } String readUrlString =
	 * baseUrl.toString() + "?$filter=mandt%20eq%20'" + mandt +
	 * "'%20and%20filePath%20eq%20'" + urlEncodedPath + "'"; InputStream
	 * retrievedData = execute(readUrlString, APPLICATION_JSON,
	 * HTTP_METHOD_GET); InputStreamReader reader = new
	 * InputStreamReader(retrievedData); BufferedReader bufferedReader = new
	 * BufferedReader(reader);
	 * 
	 * EdmEntityContainer container = edm.getDefaultEntityContainer();
	 * 
	 * ODataFeed feed = EntityProvider.readFeed(APPLICATION_JSON,
	 * container.getEntitySet(prototypeField.getEntitySet()), retrievedData,
	 * EntityProviderReadProperties.init().build());
	 * 
	 * feedbackStream.println("trovati " + found.size() +
	 * " record gi� caricati per file " + path + " mandt " + mandt); return
	 * found; }
	 */

	/*
	 * private URL retrieveBaseUrlFromEntitiesUrl(URL entitiesUrl) throws
	 * MalformedURLException { URL entityUrl =
	 * createSigleEntityBaseUrlFromGivenUrl(entitiesUrl); String stringUrl =
	 * entityUrl.toString(); int lastBar = stringUrl.lastIndexOf("/"); String
	 * baseUrlString = stringUrl.substring(0, lastBar);// tolgo la barra // che
	 * // eventualmente // verr� messa // ai livelli // superiori return new
	 * URL(baseUrlString); }
	 */

	/*
	 * private String retrieveEntitySetFromUrl(URL url) throws
	 * MalformedURLException { URL entityUrl =
	 * createSigleEntityBaseUrlFromGivenUrl(url); String urlString =
	 * entityUrl.toString(); int lastBar = urlString.lastIndexOf("/"); String
	 * afterBar = urlString.substring(lastBar + 1); return afterBar; }
	 * 
	 * private URL createSigleEntityBaseUrlFromGivenUrl(URL url) throws
	 * MalformedURLException { String stringUrl = url.toString(); if
	 * (stringUrl.endsWith("/")) { URL cutUrl = new URL(stringUrl.substring(0,
	 * stringUrl.length() - 1)); return cutUrl; } else { return url; } }
	 * 
	 * private URL createEntitySetUrlFromGivenUrl(URL url) throws
	 * MalformedURLException { String stringUrl = url.toString(); if
	 * (stringUrl.endsWith("/")) { return url; } else { URL completeUrl = new
	 * URL(stringUrl + "/"); return completeUrl; } }
	 */

	private InputStream execute(String relativeUri, String contentType,
			String httpMethod,String username,String password) throws IOException, URISyntaxException
	{

		HttpURLConnection connectionForRequests = initializeConnection(
				relativeUri, contentType, httpMethod,username,password);
		if (isIntoSapSession())
		{
			connectionForRequests.setRequestProperty("Cookie", calculateCookiesStringToSend());
		}
		connectionForRequests.connect();
		Map<String, List<String>> headers = connectionForRequests
				.getHeaderFields();
		Set<String> headerKeys = headers.keySet();
		List<String> httpRetrievedCookies = null;
		for (String headerKey:headerKeys)
		{
		   if ("set-cookie".equalsIgnoreCase(headerKey))
		   {
			   httpRetrievedCookies = headers.get(headerKey);
		   }
		}
		if (!isIntoSapSession())
		{
			calculateCookies(httpRetrievedCookies);
		}
		connectionForRequests = eventuallyRemakeConnection(
				connectionForRequests, contentType, httpMethod,username,password, true);
		checkStatus(connectionForRequests);

		InputStream content = connectionForRequests.getInputStream();
		// content = logRawContent(httpMethod + " request on uri '" +
		// relativeUri + "' with content:\n  ", content, "\n");
		return content;
	}

	private HttpURLConnection eventuallyRemakeConnection(
			HttpURLConnection connectionForRequests, String contentType,
			String httpMethod,String username,String password) throws IOException, URISyntaxException
	{
		return eventuallyRemakeConnection(connectionForRequests, contentType,
				httpMethod,username,password, true);
	}

	private HttpURLConnection eventuallyRemakeConnection(
			HttpURLConnection connectionForRequests, String contentType,
			String httpMethod,String username,String password, boolean connectOnRemake) throws IOException, URISyntaxException
	{
		if (firstConnectionUrl == null)
		{
			firstConnectionUrl = connectionForRequests.getURL();
		}
		HttpStatusCodes httpStatusCode = HttpStatusCodes
				.fromStatusCode(connectionForRequests.getResponseCode());
		if (httpStatusCode.getStatusCode() == REDIRECT_SAP_REQUEST)
		{
			Map<String, List<String>> headers = connectionForRequests
					.getHeaderFields();
			if (headers.containsKey("Location"))
			{
				List<String> locationList = headers.get("Location");
				String location = locationList.get(0);
				connectionForRequests.disconnect();
				connectionForRequests = initializeConnection(location,
						contentType, httpMethod,username,password);
				if (isIntoSapSession())
				{
					connectionForRequests.setRequestProperty("Cookie", calculateCookiesStringToSend());
				}
				if (connectOnRemake)
				{
					connectionForRequests.connect();
				}
				Set<String> headerKeys = headers.keySet();
				List<String> httpRetrievedCookies = null;
				for (String headerKey:headerKeys)
				{
				   if ("set-cookie".equalsIgnoreCase(headerKey))
				   {
					   httpRetrievedCookies = headers.get(headerKey);
				   }
				}
				if (!isIntoSapSession())
				{
					calculateCookies(httpRetrievedCookies);
				}
				redirectionUrl = connectionForRequests.getURL();
			}
			return connectionForRequests;
		} else
		{
			return connectionForRequests;
		}
	}

	private HttpStatusCodes checkStatus(HttpURLConnection connection)
			throws IOException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		HttpStatusCodes httpStatusCode = HttpStatusCodes
				.fromStatusCode(connection.getResponseCode());
		if (400 <= httpStatusCode.getStatusCode()
				&& httpStatusCode.getStatusCode() <= 599) { throw new RuntimeException(
				String.format(messages
						.getString("HTTP_CONNECTION_FAILED_WITH_STATUS_CODE"),
						httpStatusCode.getStatusCode(), httpStatusCode
								.toString())); }
		return httpStatusCode;
	}

	private HttpURLConnection initializeConnection(String absolutUri,
			String contentType, String httpMethod,String username,String password)
			throws MalformedURLException, IOException
	{
		URL url = new URL(absolutUri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod(httpMethod);
		connection.setRequestProperty(HTTP_HEADER_ACCEPT, contentType);
		if (username != null)
		{
			if (password != null)
			{
				String userpass = username + ":" + password;
				Encoder base64 = Base64.getEncoder();
				String basicAuth = "Basic " + new String(base64.encode(userpass.getBytes()));
				connection.setRequestProperty ("Authorization", basicAuth);
			}
		}
		if (HTTP_METHOD_POST.equals(httpMethod)
				|| HTTP_METHOD_PUT.equals(httpMethod))
		{
			connection.setDoOutput(true);
			connection
					.setRequestProperty(HTTP_HEADER_CONTENT_TYPE, contentType);
		}

		return connection;
	}

	public void uploadSingleFileData(List<ExtendedCsvField> singleFileData,
			Logger logger, Edm edm) throws IOException, URISyntaxException
	{
		ExtendedCsvField first = singleFileData.get(0);
		Integer maxSingleUpload = first.getMaxSingleUpload();
		if(maxSingleUpload == null)
		{
			uploadSingleFileDataBulk(singleFileData, logger, edm,false);
		}else
		{
		   uploadSingleFileDataDivided(singleFileData, logger, edm, maxSingleUpload);
		}
		
	}
	
	protected void uploadSingleFileDataDivided(List<ExtendedCsvField> singleFileData,
			Logger logger, Edm edm,Integer maxSingleUpload) throws IOException, URISyntaxException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		ExtendedCsvField first = singleFileData.get(0);
		String firstMandt = first.getMandt();
		String firstFilePathOrName = first.getFilePath();
		if (first.getStoreEncodedPath().booleanValue() == false)
		{
			firstFilePathOrName = first.getFileName();
		}
		int numberOfRecords = singleFileData.size();
		logger.log(Level.INFO, String.format(
				messages.getString("UPLOAD_N_RECORDS_FROM_FILE_WITH_MANDT"),
				numberOfRecords, firstFilePathOrName, firstMandt));
		if (maxSingleUpload <= 0)
		{
			//errore, non pu� essere 0 
			throw new IllegalArgumentException("il parametro MAX_SINGLE_UPLOAD deve essere maggiore di zero");//TODO localizzare
		}else
		{
			int nextToUpload = 0;
			while (nextToUpload < singleFileData.size())
			{
				int uploading = nextToUpload;
				int startPos = uploading;
				List<ExtendedCsvField> toUpload = new ArrayList<ExtendedCsvField>();
				for (int i=0;i<maxSingleUpload;i++)
				{
					uploading = nextToUpload;
					toUpload.add(singleFileData.get(uploading));
					nextToUpload ++;
					if (nextToUpload >= singleFileData.size())
					{
						break;
					}
				}
				int endPos = uploading;
				logger.log(Level.INFO,String.format(
						"caricamento record del file %1s da %2d a %3d con mandt %4s",firstFilePathOrName,startPos,endPos, firstMandt));//TODO localizzare
				uploadSingleFileDataBulk(toUpload, logger, edm,true);
			}
			
			logger.log(Level.INFO, String.format(messages
					.getString("ENDED_UPLOAD_N_RECORDS_FROM_FILE_WITH_MANDT"),
					numberOfRecords, firstFilePathOrName, firstMandt));
			
		}
	}
	
	protected void uploadSingleFileDataBulk(List<ExtendedCsvField> singleFileData,
			Logger logger, Edm edm, boolean divided) throws IOException, URISyntaxException
	{
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		ExtendedCsvField first = singleFileData.get(0);
		String firstMandt = first.getMandt();
		String firstFilePathOrName = first.getFilePath();
		String filePath = firstFilePathOrName;
		String username = first.getUsername();
		String password = first.getPassword();
		if (first.getStoreEncodedPath().booleanValue() == false)
		{
			firstFilePathOrName = first.getFileName();
		}
		int numberOfRecords = singleFileData.size();
		if (!divided)
		{
			logger.log(Level.INFO, String.format(
					messages.getString("UPLOAD_N_RECORDS_FROM_FILE_WITH_MANDT"),
					numberOfRecords, firstFilePathOrName, firstMandt));
		}
		
		URL uploadUrl = first.getUpdateMassivelyServletUrl();
		if (uploadUrl != null)
		{
			List<ToLoadCsvField> toLoad = new ArrayList<ToLoadCsvField>();
			for (ExtendedCsvField field : singleFileData)
			{
				if (!field.getMandt().equals(firstMandt)) { throw new IllegalArgumentException(
						"La lista fornita non contiene campi con lo stesso mandt"); }// non
																						// urgenti
																						// da
																						// tradurre
																						// perch�
																						// in
																						// teoria
																						// non
																						// dovrebbe
																						// accadere
																						// mai
				if (!field.getFilePath().equals(filePath)) { throw new IllegalArgumentException(
						"La lista fornita non contiene campi con lo stesso percorso del file"); }// non
																									// urgenti
																									// da
																									// tradurre
																									// perch�
																									// in
																									// teoria
																									// non
																									// dovrebbe
																									// accadere
																									// mai
				ToLoadCsvField fieldToLoad = field.getToLoadCsvField();
				if (field.getStoreEncodedPath())
				{
					// devo inserire in tal caso come campo il path codificato
					fieldToLoad.setFilePath(field
							.getFilePathBase64EncodedSubstitutingPipe());
				} else
				{
					fieldToLoad.setFilePath(field.getFileName());
				}
				toLoad.add(fieldToLoad);
			}
			Gson gson = new Gson();// creo l'array json
			String jsonToUpload = gson.toJson(toLoad);
			HttpURLConnection connection = initializeConnection(
					uploadUrl.toString(), APPLICATION_JSON, HTTP_METHOD_POST,username,password);
			//System.out.println(jsonToUpload);
			
		    byte[] utf8Encoded = jsonToUpload.getBytes("UTF8");
			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(utf8Encoded.length));
			if (isIntoSapSession())
			{
				connection.setRequestProperty("Cookie", calculateCookiesStringToSend());
			}
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			
			// connection.connect();
			// connection = eventuallyRemakeConnection(connection,
			// APPLICATION_JSON, HTTP_METHOD_POST);

			// Send request
			// connection.connect();
			DataOutputStream wr = new DataOutputStream(
					connection.getOutputStream());
			wr.write(utf8Encoded);
			wr.flush();
			wr.close();
			Map<String, List<String>> headers = connection
					.getHeaderFields();
			Set<String> headerKeys = headers.keySet();
			List<String> httpRetrievedCookies = null;
			for (String headerKey:headerKeys)
			{
			   if ("set-cookie".equalsIgnoreCase(headerKey))
			   {
				   httpRetrievedCookies = headers.get(headerKey);
			   }
			}
			if (!isIntoSapSession())
			{
				calculateCookies(httpRetrievedCookies);
			}
			InputStream responseStream = connection.getInputStream();// bisogna
																		// avere
																		// attivi
																		// entrambi
																		// gli
																		// stream,
																		// se no
																		// non
																		// va il
																		// caricamento
			InputStreamReader reader = new InputStreamReader(responseStream);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject) parser.parse(reader);
			JsonElement writtenValues = json.get("writtenValues");// se la
																	// risposta
																	// �
																	// corretta,
																	// dovrebbe
			// essere incapsulata in un oggetto
			// d
			if (writtenValues != null)
			{
				BigInteger intResponse = writtenValues.getAsBigInteger();
				logger.log(Level.INFO, String.format(
						messages.getString("FIRST_TRIAL_WRITTEN_N_CSV_FIELDS"),
						intResponse));
			}
			connection.disconnect();
			// verifico se mi ha mandato una redirect, in tal caso devo ricreare
			// la connessione
			HttpURLConnection remade = eventuallyRemakeConnection(connection,
					APPLICATION_JSON, HTTP_METHOD_POST,username,password, false);
			if (remade != connection)
			{
				// vuol dire che ho avuto un redirect, e quindi devo rifare
				connection = remade;
				wr = new DataOutputStream(connection.getOutputStream());
				wr.writeBytes(jsonToUpload);
				wr.flush();
				wr.close();
				responseStream = connection.getInputStream();// bisogna
																// avere
																// attivi
																// entrambi
																// gli
																// stream,
																// se
																// no
																// non
																// va
																// il
																// caricamento
				reader = new InputStreamReader(responseStream);
				parser = new JsonParser();
				json = (JsonObject) parser.parse(reader);
				writtenValues = json.get("writtenValues");// se la risposta �
															// corretta,
															// dovrebbe
				// essere incapsulata in un oggetto
				// d
				if (writtenValues != null)
				{
					BigInteger intResponse = writtenValues.getAsBigInteger();
					logger.log(Level.INFO, String.format(
									messages.getString("SECOND_TRIAL_CAUSE_OF_REDIRECT_WRITTEN_N_CSV_FIELDS"),
									intResponse));
				}
				connection.disconnect();
			}
			/*
			 * OutputStream uploadStream = connection.getOutputStream();
			 * PrintWriter writer = new PrintWriter(uploadStream);
			 * writer.print(jsonToUpload); writer.flush(); writer.close();
			 */
		} else
		{
			throw new UnsupportedOperationException(
					messages.getString("UPLOAD_CURRENTLY_MANAGED_ONLY_WITH_MASSIVE_SERVLET"));
		}

		if (!divided)
		{
			logger.log(Level.INFO, String.format(messages
					.getString("ENDED_UPLOAD_N_RECORDS_FROM_FILE_WITH_MANDT"),
					numberOfRecords, firstFilePathOrName, firstMandt));
		}
		
	}

	private List<List<ExtendedCsvField>> divideByFileAndMandt(
			List<ExtendedCsvField> originalList)
	{
		List<List<ExtendedCsvField>> toReturn = new ArrayList<List<ExtendedCsvField>>();
		for (ExtendedCsvField examiningField : originalList)
		{
			List<ExtendedCsvField> destinationList = findEventuallyCorrespondingList(
					toReturn, examiningField);
			if (destinationList == null)
			{
				destinationList = new ArrayList<ExtendedCsvField>();
				toReturn.add(destinationList);
			}
			destinationList.add(examiningField);
		}
		return toReturn;
	}

	private List<ExtendedCsvField> findEventuallyCorrespondingList(
			List<List<ExtendedCsvField>> dividedLists,
			ExtendedCsvField placingField)
	{
		for (List<ExtendedCsvField> containedList : dividedLists)
		{
			if (containedList.size() > 0)
			{
				ExtendedCsvField first = containedList.get(0);
				if (first.getMandt().equals(placingField.getMandt()))
				{
					if (first.getFilePath().equals(placingField.getFilePath())) { return containedList; }
				}
			}
		}
		return null;
	}
}
