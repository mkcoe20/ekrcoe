package it.ekr.csvuploader.engine;

import it.ekr.csvuploader.engine.csvfiledata.CsvFileOrDirDescriptor;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.*;

/**
 * Classe riservata a tutti i dati che vengono passati all'engine e che poi da
 * esso sono restituiti
 * 
 * @author Marco Scarpa
 *
 */
public class CsvUploaderEngineContents
{
	private URL url = null;
	private Collection<CsvFileOrDirDescriptor> filesList = null;
	private Collection<String> defaultKeys = null;
	private Collection<String> defaultExtensions = null;
	private String defaultFieldsSeparator = null;
	private String defaultFieldsEncloser = null;
	private Boolean defaultSubdirsSearch = null;
	private String defaultMandt = null;
	private Boolean defaultStorePathsEncoded = null;
	private String entitySet = null;
	private String deleteMassivelyFunction = null;
	private String odataServlet = null;
	private String updateMassivelyServlet = null;
	private String deleteMassivelyServlet = null;
	private Boolean useServletToDelete = null;
	private String username = null;
	private String password = null;
	private Integer maxSingleUpload = null;
	private File logFile = null;

	/**
	 * @return the url
	 */
	public URL getUrl()
	{
		return this.url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(URL url)
	{
		this.url = url;
	}

	/**
	 * @return the csvFiles
	 */
	public Collection<CsvFileOrDirDescriptor> getFilesList()
	{
		return this.filesList;
	}

	/**
	 * @param csvFiles
	 *            the csvFiles to set
	 */
	public void setFilesList(Collection<CsvFileOrDirDescriptor> csvFiles)
	{
		this.filesList = csvFiles;
	}

	/**
	 * la lista dei files csv formattata in json
	 * 
	 * @return un vettore json con i dati dei files csv da elaborare
	 */
	public String getJsonFormattedFilesList()
	{
		if (filesList != null)
		{
			Gson gson = new Gson();
			String toReturn = gson.toJson(filesList);
			return toReturn;
		}
		return null;
	}

	/**
	 * la lista dei files csv formattata in json
	 * 
	 * @param jsonFormattedCsvFiles
	 *            una stringa che corrisponde a un vettore json contenente i
	 *            dati su come elaborare i csv
	 */
	public void setJsonFormattedFilesList(String jsonFormattedCsvFiles)
	{
		if (jsonFormattedCsvFiles != null)
		{
			Gson gson = new Gson();
			Type type = new TypeToken<Collection<CsvFileOrDirDescriptor>>()
			{
			}.getType();
			filesList = gson.fromJson(jsonFormattedCsvFiles, type);
		} 

	}

	/**
	 * @return the defaultKeys
	 */
	public Collection<String> getDefaultKeys()
	{
		return this.defaultKeys;
	}

	/**
	 * @param defaultKeys
	 *            the defaultKeys to set
	 */
	public void setDefaultKeys(Collection<String> defaultKeys)
	{
		this.defaultKeys = defaultKeys;
	}

	/**
	 * la lista delle chiavi di default cercate su tutti i files salvo impostazione specifica per il file formattata in json
	 * @return un vettore json di stringhe che indica le chiavi di default cercate nei files
	 */
	public String getJsonFormattedDefaultKeys()
	{

		if (defaultKeys != null)
		{
			Gson gson = new Gson();
			String toReturn = gson.toJson(defaultKeys);
			return toReturn;
		}
		return null;
	}

	
	/**
	 * la lista delle chiavi di default cercate su tutti i files salvo impostazione specifica per il  file formattata in json
	 * @param jsonFormattedKeys una stringa che rappresenta un vettore formattato in json che indica le chiavi cercate nei files salvo impostazioni specifiche per i files
	 */
	public void setJsonFormattedDefaultKeys(String jsonFormattedKeys)
	{
		if (jsonFormattedKeys != null)
		{
			Gson gson = new Gson();
			Type type = new TypeToken<Collection<String>>()
			{
			}.getType();
			defaultKeys = gson.fromJson(jsonFormattedKeys, type);
		}
	}

	/**
	 * @return the defaultExtensions
	 */
	public Collection<String> getDefaultExtensions()
	{
		return this.defaultExtensions;
	}

	/**
	 * @param defaultExtensions the defaultExtensions to set
	 */
	public void setDefaultExtensions(Collection<String> defaultExtensions)
	{
		this.defaultExtensions = defaultExtensions;
	}
	
	public String getJsonFormattedDefaultExtensions()
	{
		if (defaultExtensions != null)
		{
			Gson gson = new Gson();
			String toReturn = gson.toJson(defaultExtensions);
			return toReturn;
		}
		return null;
	}

	
	public void setJsonFormattedDefaultExtensions(String jsonFormattedDefaultExtensions)
	{
		if (jsonFormattedDefaultExtensions != null)
		{
			Gson gson = new Gson();
			Type type = new TypeToken<Collection<String>>()
			{}.getType();
			defaultExtensions = gson.fromJson(jsonFormattedDefaultExtensions, type);
		}
	}

	/**
	 * @return the defaultFieldsSeparator
	 */
	public String getDefaultFieldsSeparator()
	{
		return this.defaultFieldsSeparator;
	}

	/**
	 * @param defaultFieldsSeparator the defaultFieldsSeparator to set
	 */
	public void setDefaultFieldsSeparator(String defaultFieldsSeparator)
	{
		this.defaultFieldsSeparator = defaultFieldsSeparator;
	}

	/**
	 * @return the defaultFieldsEncloser
	 */
	public String getDefaultFieldsEncloser()
	{
		return this.defaultFieldsEncloser;
	}

	/**
	 * @param defaultFieldsEncloser the defaultFieldsEncloser to set
	 */
	public void setDefaultFieldsEncloser(String defaultFieldsEncloser)
	{
		this.defaultFieldsEncloser = defaultFieldsEncloser;
	}

	/**
	 * @return the defaultSubdirsSearch
	 */
	public Boolean getDefaultSubdirsSearch()
	{
		return this.defaultSubdirsSearch;
	}

	/**
	 * @param defaultSubdirsSearch the defaultSubdirsSearch to set
	 */
	public void setDefaultSubdirsSearch(Boolean defaultSubdirsSearch)
	{
		this.defaultSubdirsSearch = defaultSubdirsSearch;
	}

	/**
	 * @return the defaultMandt
	 */
	public String getDefaultMandt()
	{
		return this.defaultMandt;
	}

	/**
	 * @param defaultMandt the defaultMandt to set
	 */
	public void setDefaultMandt(String defaultMandt)
	{
		this.defaultMandt = defaultMandt;
	}

	/**
	 * @return the defaultStorePathsEncoded
	 */
	public Boolean getDefaultStorePathsEncoded()
	{
		return this.defaultStorePathsEncoded;
	}

	/**
	 * @param defaultStorePathsEncoded the defaultStorePathsEncoded to set
	 */
	public void setDefaultStorePathsEncoded(Boolean defaultStorePathsEncoded)
	{
		this.defaultStorePathsEncoded = defaultStorePathsEncoded;
	}

	/**
	 * @return the entitySet
	 */
	public String getEntitySet()
	{
		return this.entitySet;
	}

	/**
	 * @param entitySet the entitySet to set
	 */
	public void setEntitySet(String entitySet)
	{
		this.entitySet = entitySet;
	}

	/**
	 * @return the deleteMassivelyFunction
	 */
	public String getDeleteMassivelyFunction()
	{
		return this.deleteMassivelyFunction;
	}

	/**
	 * @param deleteMassivelyFunction the deleteMassivelyFunction to set
	 */
	public void setDeleteMassivelyFunction(String deleteMassivelyFunction)
	{
		this.deleteMassivelyFunction = deleteMassivelyFunction;
	}

	/**
	 * @return the odataServlet
	 */
	public String getOdataServlet()
	{
		return this.odataServlet;
	}

	/**
	 * @param odataServlet the odataServlet to set
	 */
	public void setOdataServlet(String odataServlet)
	{
		this.odataServlet = odataServlet;
	}

	/**
	 * @return the updateMassivelyServlet
	 */
	public String getUpdateMassivelyServlet()
	{
		return this.updateMassivelyServlet;
	}

	/**
	 * @param updateMassivelyServlet the updateMassivelyServlet to set
	 */
	public void setUpdateMassivelyServlet(String updateMassivelyServlet)
	{
		this.updateMassivelyServlet = updateMassivelyServlet;
	}

	/**
	 * @return the deleteMassivelyServlet
	 */
	public String getDeleteMassivelyServlet()
	{
		return this.deleteMassivelyServlet;
	}

	/**
	 * @param deleteMassivelyServlet the deleteMassivelyServlet to set
	 */
	public void setDeleteMassivelyServlet(String deleteMassivelyServlet)
	{
		this.deleteMassivelyServlet = deleteMassivelyServlet;
	}

	/**
	 * @return the useServletToDelete
	 */
	public Boolean getUseServletToDelete()
	{
		return this.useServletToDelete;
	}

	/**
	 * @param useServletToDelete the useServletToDelete to set
	 */
	public void setUseServletToDelete(Boolean useServletToDelete)
	{
		this.useServletToDelete = useServletToDelete;
	}

	/**
	 * @return the username
	 */
	public String getUsername()
	{
		return this.username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username)
	{
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return this.password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the maxSingleUpload
	 */
	public Integer getMaxSingleUpload()
	{
		return this.maxSingleUpload;
	}

	/**
	 * @param maxSingleUpload the maxSingleUpload to set
	 */
	public void setMaxSingleUpload(Integer maxSingleUpload)
	{
		this.maxSingleUpload = maxSingleUpload;
	}

	/**
	 * @return the logFile
	 */
	public File getLogFile()
	{
		return this.logFile;
	}

	/**
	 * @param logFile the logFile to set
	 */
	public void setLogFile(File logFile)
	{
		this.logFile = logFile;
	}

}
