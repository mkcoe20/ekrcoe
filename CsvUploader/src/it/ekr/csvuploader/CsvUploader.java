package it.ekr.csvuploader;

import it.ekr.csvuploader.engine.CsvUploaderEngine;
import it.ekr.csvuploader.engine.CsvUploaderEngineContents;
import it.ekr.csvuploader.engine.csvfiledata.CsvFileOrDirDescriptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import org.apache.commons.cli.*;
import org.apache.olingo.odata2.api.exception.ODataException;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class CsvUploader
{

	public static final String FALSE_STRING = "false";
	public static final String TRUE_STRING = "true";
	public static final String JSON_VECTOR_STARTING = "[";
	public static final String DEFAULT_CONFIG_FILE_NAME = "config.config";
	public static final String CONFIG_FILE_PROPERTY_FILE = "config";
	public static final String CONFIG_FILE_OPTION_STRING = "config";
	public static final String CONFIG_FILE_OPTION_CONFIG_FILE_PATH_ARGUMENT = "config_file_path";
	public static final String HELP_OPTION_STRING = "?";
	public static final String URL_FIELD = "URL";
	public static final String FILES_FIELD = "FILES";
	public static final String DEFAULT_KEYS_FIELD = "DEFAULT_KEYS";
	public static final String DEFAULT_EXTENSIONS_FIELD = "DEFAULT_EXTENSIONS";
	public static final String DEFAULT_FIELDS_SEPARATOR_FIELD = "DEFAULT_FIELDS_SEPARATOR";
	public static final String DEFAULT_FIELDS_ENCLOSER_FIELD = "DEFAULT_FIELDS_ENCLOSER";
	public static final String DEFAULT_SUBDIRS_SEARCH_FIELD = "DEFAULT_SUBDIRS_SEARCH";
	public static final String DEFAULT_MANDT_FIELD = "DEFAULT_MANDT";
	public static final String DEFAULT_STORE_PATHS_ENCODED_FIELD = "DEFAULT_STORE_PATHS_ENCODED";
	public static final String ENTITY_SET_FIELD = "ENTITY_SET";
	public static final String DELETE_MASSIVELY_FUNCTION_FIELD = "DELETE_MASSIVELY_FUNCTION";
	public static final String ODATA_SERVLET_FIELD = "ODATA_SERVLET";
	public static final String UPDATE_MASSIVELY_SERVLET_FIELD = "UPDATE_MASSIVELY_SERVLET";
	public static final String DELETE_MASSIVELY_SERVLET_FIELD = "DELETE_MASSIVELY_SERVLET";
	public static final String USE_SERVLET_TO_DELETE_FIELD = "USE_SERVLET_TO_DELETE";
	public static final String USERNAME_FIELD = "USERNAME";
	public static final String PASSWORD_FIELD = "PASSWORD";
	public static final String MAX_SINGLE_UPLOAD_FIELD = "MAX_SINGLE_UPLOAD";
	public static final String LOG_FILE_FIELD = "LOG_FILE";
	
	

	public static void main(String[] args)
	{

		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());

		Options commandLineOptions = new Options();
		Option helpOption = OptionBuilder.withDescription(
				messages.getString("SHOWS_THIS_HELP")).create(
				HELP_OPTION_STRING);
		Option configFileOption = OptionBuilder
				.withArgName(CONFIG_FILE_OPTION_CONFIG_FILE_PATH_ARGUMENT)
				.hasArg()
				.withDescription(
						String.format(messages
								.getString("CONFIG_FILE_OPTION_DESCRIPTION"),
								DEFAULT_CONFIG_FILE_NAME))
				.create(CONFIG_FILE_OPTION_STRING);
		commandLineOptions.addOption(helpOption);
		commandLineOptions.addOption(configFileOption);

		CommandLineParser parser = new BasicParser();

		try
		{
			CommandLine commandLine = parser.parse(commandLineOptions, args);
			File configFile = null;
			if (commandLine.hasOption(CONFIG_FILE_OPTION_STRING))
			{
				String configFilePath = commandLine
						.getOptionValue(CONFIG_FILE_OPTION_STRING);
				configFile = new File(configFilePath);
				if (configFile.exists())
				{
					// devo caricare i valori del file config e proseguire
					if (configFile.isDirectory())
					{
						Path configFileDirPath = configFile.toPath();
						Path configFilePathResolved = configFileDirPath
								.resolve(DEFAULT_CONFIG_FILE_NAME);
						configFile = configFilePathResolved.toFile();
						if (configFile.exists())
						{
							// devo gestire il file di configurazione e
							// proseguire
							interpretConfigFile(configFile, null);
						} else
						{
							// devo gestire il fallback su file della directory
							// corrente e salvare i dati sul file fornito
							configFile = fallBackToCurrentDirectoryFileOrHardCoded();
							File savingFile = configFilePathResolved.toFile();
							interpretConfigFile(configFile, savingFile);
						}
					} else
					{
						// ne ho gi� verificato l'esistenza quindi devo solo
						// caricare il file
						interpretConfigFile(configFile, null);
					}

				} else
				{
					if (configFilePath.endsWith(File.separator))
					{
						// sto cercando di creare una directory e quindi il file
						// generato deve avere il nome di default
						configFilePath = configFilePath
								+ DEFAULT_CONFIG_FILE_NAME;
					}
					// devo gestire il fallback su file della directory corrente
					// e salvare i dati sul file fornito
					configFile = fallBackToCurrentDirectoryFileOrHardCoded();
					File savingFile = new File(configFilePath);
					interpretConfigFile(configFile, savingFile);

				}
			} else
			{
				configFile = retrieveCurrentDirFile();
				if (configFile.exists())
				{
					// devo elaborarlo
					interpretConfigFile(configFile, null);
				} else
				{
					File savingFile = configFile;
					configFile = fallBackToCurrentDirectoryFileOrHardCoded();
					interpretConfigFile(configFile, savingFile);
				}

			}
			if (commandLine.hasOption(HELP_OPTION_STRING))
			{
				showHelp(commandLineOptions);
			}
		} catch (ParseException | URISyntaxException | IOException | CloneNotSupportedException | ODataException e)
		{
		
			//visto che i metodi che interpretano i files di configurazione dovrebbero essere abbastanza sicuri,il logger dovrebbe gi� essere inizisalizzato orima dell'eccezione
			Logger logger = Logger.getLogger(CsvUploader.class.getName());
			logger.log(Level.SEVERE, null, e);
			
		}

	}

	private static File fallBackToCurrentDirectoryFileOrHardCoded()
			throws URISyntaxException, IOException
	{
		File configFile = retrieveCurrentDirFile();
		if (configFile.exists())
		{
			return configFile;
		} else
		{
			return fallBackToHardCodedFile();
		}

	}

	/**
	 * @return
	 */
	private static File retrieveCurrentDirFile()
	{
		String defaultConfigFilePath = DEFAULT_CONFIG_FILE_NAME;
		File configFile = new File(defaultConfigFilePath);
		return configFile;
	}

	/**
	 * @return
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private static File fallBackToHardCodedFile() throws URISyntaxException,
			IOException
	{
		ResourceBundle bundle = ResourceBundle
				.getBundle(CONFIG_FILE_PROPERTY_FILE);
		String url = bundle.getString(URL_FIELD);
		String filesString = retrieveFromBundleOrNull(bundle, FILES_FIELD);

		String defaultKeys = retrieveFromBundleOrNull(bundle,
				DEFAULT_KEYS_FIELD);

		String defaultExtensions = retrieveFromBundleOrNull(bundle,
				DEFAULT_EXTENSIONS_FIELD);
		String defaultFieldsSepatator = retrieveFromBundleOrNull(bundle, DEFAULT_FIELDS_SEPARATOR_FIELD);
		String defaultFieldsEncloser = retrieveFromBundleOrNull(bundle, DEFAULT_FIELDS_ENCLOSER_FIELD);
		Boolean defaultSubdirsSearch = null;
		String defaultSubdirsSearchFieldString = retrieveFromBundleOrNull(bundle, DEFAULT_SUBDIRS_SEARCH_FIELD);
		defaultSubdirsSearch = convertStringToBooleanObject(defaultSubdirsSearchFieldString);
		String defaultMandt = retrieveFromBundleOrNull(bundle, DEFAULT_MANDT_FIELD);
		Boolean defaultStorePathsEncoded = null;
		String defaultStorePathsEncodedString = retrieveFromBundleOrNull(bundle, DEFAULT_STORE_PATHS_ENCODED_FIELD);
		defaultStorePathsEncoded = convertStringToBooleanObject(defaultStorePathsEncodedString);
		String entitySet = retrieveFromBundleOrNull(bundle, ENTITY_SET_FIELD);
		String deleteMassivelyFunction = retrieveFromBundleOrNull(bundle, DELETE_MASSIVELY_FUNCTION_FIELD);
		String odataServlet = retrieveFromBundleOrNull(bundle, ODATA_SERVLET_FIELD);
		String updateMassivelyServlet = retrieveFromBundleOrNull(bundle, UPDATE_MASSIVELY_SERVLET_FIELD);
		String deleteMassivelyServlet = retrieveFromBundleOrNull(bundle, DELETE_MASSIVELY_SERVLET_FIELD);
		String useServletToDeleteString = retrieveFromBundleOrNull(bundle, USE_SERVLET_TO_DELETE_FIELD);
		Boolean useServletToDelete = convertStringToBooleanObject(useServletToDeleteString);
		String username = retrieveFromBundleOrNull(bundle, USERNAME_FIELD);
		String password = retrieveFromBundleOrNull(bundle, PASSWORD_FIELD);
		String maxSingleUploadString = retrieveFromBundleOrNull(bundle, MAX_SINGLE_UPLOAD_FIELD);
		String logFileString = retrieveFromBundleOrNull(bundle, LOG_FILE_FIELD);
		
		File creating = new File(DEFAULT_CONFIG_FILE_NAME);
		Properties properties = new Properties();
		properties.setProperty(URL_FIELD, url);
		insertInPropertiesIfNotNull(filesString, properties, FILES_FIELD, true);
		insertInPropertiesIfNotNull(defaultKeys, properties, DEFAULT_KEYS_FIELD, true);
		insertInPropertiesIfNotNull(defaultExtensions, properties,
				DEFAULT_EXTENSIONS_FIELD, true);
		insertInPropertiesIfNotNull(defaultFieldsSepatator, properties, DEFAULT_FIELDS_SEPARATOR_FIELD, true);
		insertInPropertiesIfNotNull(defaultFieldsEncloser, properties, DEFAULT_FIELDS_ENCLOSER_FIELD, true);
		insertInPropertiesIfNotNull(defaultSubdirsSearchFieldString, properties, DEFAULT_SUBDIRS_SEARCH_FIELD, true);
		insertInPropertiesIfNotNull(defaultMandt, properties, DEFAULT_MANDT_FIELD);
		insertInPropertiesIfNotNull(defaultStorePathsEncodedString, properties, DEFAULT_STORE_PATHS_ENCODED_FIELD);
		insertInPropertiesIfNotNull(entitySet, properties, ENTITY_SET_FIELD);
		insertInPropertiesIfNotNull(deleteMassivelyFunction, properties, DELETE_MASSIVELY_FUNCTION_FIELD);
		insertInPropertiesIfNotNull(odataServlet, properties, ODATA_SERVLET_FIELD);
		insertInPropertiesIfNotNull(updateMassivelyServlet, properties, UPDATE_MASSIVELY_SERVLET_FIELD);
		insertInPropertiesIfNotNull(deleteMassivelyServlet, properties, DELETE_MASSIVELY_SERVLET_FIELD);
		insertInPropertiesIfNotNull(useServletToDeleteString, properties, USE_SERVLET_TO_DELETE_FIELD);
		insertInPropertiesIfNotNull(username, properties, USERNAME_FIELD);
		insertInPropertiesIfNotNull(password, properties, PASSWORD_FIELD);
		insertInPropertiesIfNotNull(maxSingleUploadString, properties, MAX_SINGLE_UPLOAD_FIELD);
		insertInPropertiesIfNotNull(logFileString, properties, LOG_FILE_FIELD);

		FileOutputStream out = new FileOutputStream(creating);
		ResourceBundle messages = ResourceBundle.getBundle("messages",
				Locale.getDefault());
		properties.store(out, messages.getString("CONFIG_FILE_CREATED_AUTOMATICALLY"));
		out.close();
		return creating;
	}

	/**
	 * @param booleanString
	 */
	private static Boolean convertStringToBooleanObject(
			String booleanString)
	{
		Boolean booleanObject = null;
		if (booleanString != null)
		{
			if (booleanString.equalsIgnoreCase(TRUE_STRING))
			{
				booleanObject = true;
			}else
			{
				if (booleanString.equalsIgnoreCase(FALSE_STRING))
				{
					booleanObject = false;
				}else
				{
					//non corrispondendo a nessuna stringa nota, restituisco un null, da capire se aggiungere anche valori numerici (0 per false, altro numero per true)
					booleanObject = null;
				}
				
			}
		}else
		{
			booleanObject = null;
		}
		return booleanObject;
	}

	/**
	 * @param fieldValue
	 * @param properties
	 */
	private static void insertInPropertiesIfNotNull(String fieldValue,
			Properties properties, String field)
	{
		insertInPropertiesIfNotNull(fieldValue, properties, field, true);
	}

	/**
	 * @param fieldValue
	 * @param properties
	 * @param removeProperty se true rimuove la property se � null
	 */
	private static void insertInPropertiesIfNotNull(String fieldValue,
			Properties properties, String field, boolean removeProperty)
	{
		if (fieldValue != null)
		{
			properties.setProperty(field, fieldValue);
		}else
		{
			if (removeProperty)
			{
				if (properties.containsKey(field))
				{
					properties.remove(field);
				}
				
			}
		}
	}

	/**
	 * @param bundle
	 * @return
	 */
	private static String retrieveFromBundleOrNull(ResourceBundle bundle,
			String field)
	{
		String fieldValue = null;
		if (bundle.containsKey(field))
		{
			fieldValue = bundle.getString(field);
		}
		return fieldValue;
	}

	private static String retrieveFromPropertiesOrNull(Properties properties,
			String field)
	{

		String fieldValue = properties.getProperty(field, null);
		return fieldValue;
	}

	private static void showHelp(Options commandLineOptions)
	{
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("CsvUploader", commandLineOptions);
	}

	private static void interpretConfigFile(File configFile, File savingFile)
			throws IOException, CloneNotSupportedException, ODataException, JsonIOException, JsonSyntaxException, UnsupportedOperationException, URISyntaxException
	{
		Properties configFileProperties = new Properties();
		FileInputStream in = new FileInputStream(configFile);
		configFileProperties.load(in);
		File logFile = null;//per prima cosa inizializzo il log se si pu�
		String logFileString = retrieveFromPropertiesOrNull(configFileProperties, LOG_FILE_FIELD);
		if (logFileString != null)
		{
			logFile = new File(logFileString);
		}
		Logger logger = Logger.getLogger(CsvUploader.class.getName());
		if (logFile != null)
		{
			FileOutputStream logFileStream = new FileOutputStream(logFile, true);//vado in append nel file corrente
			PrintStream logFilePrintStream = new PrintStream(logFileStream);
			logFilePrintStream.println();//aggiungo un a capo per sicurezza
			StreamHandler loggerHandler = new StreamHandler(logFilePrintStream,new SimpleFormatter());
			logger.addHandler(loggerHandler);
		}
		String urlString = configFileProperties.getProperty(URL_FIELD);
		String filesString = retrieveFromPropertiesOrNull(configFileProperties,
				FILES_FIELD);
		String defaultKeysString = retrieveFromPropertiesOrNull(
				configFileProperties, DEFAULT_KEYS_FIELD);

		String defaultExtensionsString = retrieveFromPropertiesOrNull(
				configFileProperties, DEFAULT_EXTENSIONS_FIELD);
		
		String defaultFieldsSeparator = retrieveFromPropertiesOrNull(configFileProperties, DEFAULT_FIELDS_SEPARATOR_FIELD);
		String defaultFieldsEncloser = retrieveFromPropertiesOrNull(configFileProperties, DEFAULT_FIELDS_ENCLOSER_FIELD);

		Boolean defaultSubdirsSearch = null;
		String defaultSubdirsSearchString = retrieveFromPropertiesOrNull(configFileProperties, DEFAULT_SUBDIRS_SEARCH_FIELD);
		defaultSubdirsSearch = convertStringToBooleanObject(defaultSubdirsSearchString);
		String defaultMandt = retrieveFromPropertiesOrNull(configFileProperties, DEFAULT_MANDT_FIELD);
		
	    Boolean defaultStorePathsEncoded = null;
	    String defaultStorePathsEncodedString = retrieveFromPropertiesOrNull(configFileProperties, DEFAULT_STORE_PATHS_ENCODED_FIELD);
 		defaultStorePathsEncoded = convertStringToBooleanObject(defaultStorePathsEncodedString);
 		
 		String entitySet = retrieveFromPropertiesOrNull(configFileProperties, ENTITY_SET_FIELD);
 		String deleteMassivelyFunction = retrieveFromPropertiesOrNull(configFileProperties, DELETE_MASSIVELY_FUNCTION_FIELD);
 		String odataServlet = retrieveFromPropertiesOrNull(configFileProperties, ODATA_SERVLET_FIELD);
 		String updateMassivelyServlet = retrieveFromPropertiesOrNull(configFileProperties, UPDATE_MASSIVELY_SERVLET_FIELD);
 		String deleteMassivelyServlet = retrieveFromPropertiesOrNull(configFileProperties, DELETE_MASSIVELY_SERVLET_FIELD);
		String useServletToDeleteString = retrieveFromPropertiesOrNull(configFileProperties, USE_SERVLET_TO_DELETE_FIELD);
		Boolean useServletToDelete = convertStringToBooleanObject(useServletToDeleteString);
		String username = retrieveFromPropertiesOrNull(configFileProperties, USERNAME_FIELD);
		String password = retrieveFromPropertiesOrNull(configFileProperties, PASSWORD_FIELD);
		Integer maxSingleUpload = null;
		String maxSingleUploadString = retrieveFromPropertiesOrNull(configFileProperties, MAX_SINGLE_UPLOAD_FIELD);
		if (maxSingleUploadString != null)
		{
			maxSingleUpload = new Integer(maxSingleUploadString);
		}
		
		
 		URL url = new URL(urlString);
		CsvUploaderEngineContents configFileContents = new CsvUploaderEngineContents();
		configFileContents.setUrl(url);
		if (filesString != null)
		{
			if (filesString.startsWith(JSON_VECTOR_STARTING))
			{
				// assumo che indichi un vettore json
				configFileContents.setJsonFormattedFilesList(filesString);
			} else
			{
				// creo la lista contenente un solo file
				CsvFileOrDirDescriptor singleFile = new CsvFileOrDirDescriptor();
				singleFile.setCsvFileOrDirPath(filesString);
				//la decisione se fare o meno una ricerca ricorsiva viene presa in sede di engine e non va salvata qui se no si riporterebbe il default che potrebbe non essere quello voluto
				/*if (filesString.endsWith(File.separator))
				{
					// vuol dire che � stata indicata una directory e quindi
					// imposto che non vi si faccia ricerca ricorsiva
					singleFile.setSubdirsSearch(defaults);
				} else
				{
					// devo comunque verificare se � una directory
					File checkingIfDir = new File(filesString);
					if (checkingIfDir.exists())
					{
						if (checkingIfDir.isDirectory())
						{
							singleFile.setSubdirsSearch(false);
						}
					}
				}//*/
				List<CsvFileOrDirDescriptor> singleFileList = new ArrayList<CsvFileOrDirDescriptor>();
				singleFileList.add(singleFile);
				configFileContents.setFilesList(singleFileList);
			}
		}

		if (defaultKeysString != null)
		{
			if (defaultKeysString.startsWith(JSON_VECTOR_STARTING))
			{
				configFileContents
						.setJsonFormattedDefaultKeys(defaultKeysString);
			} else
			{
				List<String> singleKeyList = new ArrayList<String>();
				singleKeyList.add(defaultKeysString);
				configFileContents.setDefaultKeys(singleKeyList);
			}
		}

		if (defaultExtensionsString != null)
		{
			if (defaultExtensionsString.startsWith(JSON_VECTOR_STARTING))
			{
				configFileContents
						.setJsonFormattedDefaultExtensions(defaultExtensionsString);
			} else
			{
				List<String> singleExtensionList = new ArrayList<String>();
				singleExtensionList.add(defaultExtensionsString);
				configFileContents.setDefaultExtensions(singleExtensionList);
			}
		}

		configFileContents.setDefaultFieldsSeparator(defaultFieldsSeparator);
		configFileContents.setDefaultFieldsEncloser(defaultFieldsEncloser);
		configFileContents.setDefaultSubdirsSearch(defaultSubdirsSearch);
		configFileContents.setDefaultMandt(defaultMandt);
		configFileContents.setDefaultStorePathsEncoded(defaultStorePathsEncoded);
		configFileContents.setEntitySet(entitySet);
		configFileContents.setDeleteMassivelyFunction(deleteMassivelyFunction);
		configFileContents.setOdataServlet(odataServlet);
		configFileContents.setUpdateMassivelyServlet(updateMassivelyServlet);
		configFileContents.setDeleteMassivelyServlet(deleteMassivelyServlet);
		configFileContents.setUseServletToDelete(useServletToDelete);
		configFileContents.setUsername(username);
		configFileContents.setPassword(password);
		configFileContents.setMaxSingleUpload(maxSingleUpload);
		configFileContents.setLogFile(logFile);

		
		CsvUploaderEngine engine = new CsvUploaderEngine();
		
		
		configFileContents = engine.startFromEngineContents(configFileContents,logger);
	 	if (savingFile != null)
		{
			if (savingFile.exists() == false)
			{
				File parentDir = savingFile.getParentFile();
				if (!parentDir.exists())
				{
					parentDir.mkdirs();
				}
			}
			FileOutputStream out = new FileOutputStream(savingFile);
			configFileProperties.setProperty(URL_FIELD, configFileContents
					.getUrl().toString());
			insertInPropertiesIfNotNull(
					configFileContents.getJsonFormattedFilesList(),
					configFileProperties, FILES_FIELD, true);
			insertInPropertiesIfNotNull(
					configFileContents.getJsonFormattedDefaultKeys(),
					configFileProperties, DEFAULT_KEYS_FIELD, true);
			insertInPropertiesIfNotNull(
					configFileContents.getJsonFormattedDefaultExtensions(),
					configFileProperties, DEFAULT_EXTENSIONS_FIELD, true);
			insertInPropertiesIfNotNull(configFileContents.getDefaultFieldsSeparator(), configFileProperties, DEFAULT_FIELDS_SEPARATOR_FIELD, true);
			insertInPropertiesIfNotNull(configFileContents.getDefaultFieldsEncloser(), configFileProperties, DEFAULT_FIELDS_ENCLOSER_FIELD, true);
			defaultSubdirsSearchString = null;
			defaultSubdirsSearch = configFileContents.getDefaultSubdirsSearch();
			defaultSubdirsSearchString = convertBooleanObjectToString(defaultSubdirsSearch);
			insertInPropertiesIfNotNull(defaultSubdirsSearchString, configFileProperties, DEFAULT_SUBDIRS_SEARCH_FIELD, true);
			insertInPropertiesIfNotNull(configFileContents.getDefaultMandt(), configFileProperties, DEFAULT_MANDT_FIELD);
			defaultStorePathsEncodedString = null;
			defaultStorePathsEncoded = configFileContents.getDefaultStorePathsEncoded();
			defaultStorePathsEncodedString = convertBooleanObjectToString(defaultStorePathsEncoded);
			insertInPropertiesIfNotNull(defaultStorePathsEncodedString, configFileProperties, DEFAULT_STORE_PATHS_ENCODED_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getEntitySet(), configFileProperties, ENTITY_SET_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getDeleteMassivelyFunction(), configFileProperties, DELETE_MASSIVELY_FUNCTION_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getOdataServlet(), configFileProperties, ODATA_SERVLET_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getUpdateMassivelyServlet(), configFileProperties, UPDATE_MASSIVELY_SERVLET_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getDeleteMassivelyServlet(), configFileProperties, DELETE_MASSIVELY_SERVLET_FIELD);
			useServletToDelete = null;
			useServletToDelete = configFileContents.getUseServletToDelete();
			useServletToDeleteString = convertBooleanObjectToString(useServletToDelete);
			insertInPropertiesIfNotNull(useServletToDeleteString, configFileProperties, USE_SERVLET_TO_DELETE_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getUsername(), configFileProperties, USERNAME_FIELD);
			insertInPropertiesIfNotNull(configFileContents.getPassword(), configFileProperties, PASSWORD_FIELD);
			maxSingleUploadString = null;
			maxSingleUpload = configFileContents.getMaxSingleUpload();
			if (maxSingleUpload != null)
			{
				maxSingleUploadString = maxSingleUpload.toString();
			}
			insertInPropertiesIfNotNull(maxSingleUploadString, configFileProperties, MAX_SINGLE_UPLOAD_FIELD);

			//non essendo cambiata durante le elaborazioni tengo la stringa passata in modo da conservare eventuali percorsi relativi
			insertInPropertiesIfNotNull(logFileString, configFileProperties, LOG_FILE_FIELD);
			ResourceBundle messages = ResourceBundle.getBundle("messages",
					Locale.getDefault());
			configFileProperties.store(out,
					messages.getString("MADE_FROM_PROCESSED_DATA"));
			out.close();
		}

	}

	/**
	 * @param booleanObject
	 * @return 
	 */
	private static String convertBooleanObjectToString(
			Boolean booleanObject)
	{
		String booleanString;
		if (booleanObject != null)
		{
			if (booleanObject == true)
			{
				booleanString = TRUE_STRING;
			}else
			{
				booleanString = FALSE_STRING;
			}
		}else
		{
			booleanString = null;
		}
		return booleanString;
	}

}
